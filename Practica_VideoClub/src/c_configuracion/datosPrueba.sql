//INSERCIONES TABLA GENERO

INSERT INTO genero (descripcion) VALUES ('Terror');
INSERT INTO genero (descripcion) VALUES ('Aventura');
INSERT INTO genero (descripcion) VALUES ('Accion');
INSERT INTO genero (descripcion) VALUES ('Comedia');
INSERT INTO genero (descripcion) VALUES ('Suspense');

//INSERCIONES TABLA ACTORES

INSERT INTO actor (nombre,fechanac) VALUES ('Will Smith','1968-09-25');
INSERT INTO actor (nombre,fechanac) VALUES ('Brad Pitt','1963-12-18');
INSERT INTO actor (nombre,fechanac) VALUES ('Joel Kinnaman','1979-11-25');

//INSERCIONES TABLA PELICULAS

INSERT INTO pelicula (titulo,sinopsis,preciodia) VALUES ('Alicia en el Pais','Erase una vez una pelicula',5);
INSERT INTO pelicula (titulo,sinopsis,preciodia) VALUES ('Matrix','Wake up!',10);
INSERT INTO pelicula (titulo,sinopsis,preciodia) VALUES ('El viaje de Chihiro','Sinopsis profunda de la pelicula',15);

