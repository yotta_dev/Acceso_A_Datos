package a_interfaz_Grafica;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class A_Init_and_Config extends JFrame {

	private JPanel contentPane;

	// CREACION DEL OBJECTO QUE NOS DARA PERSISTENCIA MYSQL A TODO EL PROGRAMA
	public static C_Persistencia_MySQL c_Persistencia_MySQL;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A_Init_and_Config frame = new A_Init_and_Config();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);

					// Creamos Objecto Properties sobre el cual cargaremos las Propiedades de
					// Configuracion Inicial
					Properties properties = new Properties();

					// CFG.INI se buscará en el mismo paquete que la clase
					// Obtencion_Parametros_Configuracion
					properties.load(new InputStreamReader(A_Init_and_Config.class.getResourceAsStream("CFG.INI")));

					// Obtenemos el valor del tipo de Persistencia de las Propiedades cargadas sobre
					// el Objeto Propierties que han sido añadidas a traves del fichero de
					// Configuracion Inicial.
					String tipoPersistencia = properties.getProperty("tipoPersistencia");

					switch (tipoPersistencia) {

					case "mysqlJDBC":
						String server_ip = properties.getProperty("mysqlJDBC.server_ip");
						String usuario = properties.getProperty("mysqlJDBC.usuario");
						String password = properties.getProperty("mysqlJDBC.password");
						String baseDatos = properties.getProperty("mysqlJDBC.basedatos");

						c_Persistencia_MySQL = new C_Persistencia_MySQL(server_ip, usuario, password, baseDatos);

						break;

					case "hibernate":
						// RELLENAR CON PERSISTENCIA HIBERNATE
						break;

					default:
						// ERROR
					}

				} catch (Exception e) {
					// CERRAMOS EL PROGRAMA TRAS INFORMAR DEL ERROR
					mostrarMensaje_A_Usuario(e.getMessage(), "Error de Base de Datos");
					System.exit(1);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public A_Init_and_Config() {
		setResizable(false);
		setTitle("BLOCKBUSTER");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 276, 243);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("Button.focus"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton jbuttonGeneros = new JButton("Generos");
		jbuttonGeneros.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				abrirVentanaGenero();
			}
		});
		jbuttonGeneros.setBounds(77, 37, 117, 25);
		contentPane.add(jbuttonGeneros);

		JButton jbuttonPeliculas = new JButton("Peliculas");
		jbuttonPeliculas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				abrirVentanaPeliculas();
			}
		});
		jbuttonPeliculas.setBounds(77, 130, 117, 25);
		contentPane.add(jbuttonPeliculas);

		JButton jbuttonActores = new JButton("Actores");
		jbuttonActores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				abrirVentanaActores();
			}
		});
		jbuttonActores.setBounds(77, 84, 117, 25);
		contentPane.add(jbuttonActores);
	}

	private void abrirVentanaGenero() {// Rutina de apertura modal de otra ventana
		// Inhabilitamos la Ventana Actual
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setEnabled(false);
		// Y le damos paso a la siguiente Ventana, pasandole como contexto esta pestaña
		A1_Genero_Principal_GUI a_Principal_Genero_GUI = new A1_Genero_Principal_GUI(this);
		a_Principal_Genero_GUI.setVisible(true);
	}

	private void abrirVentanaActores() {// Rutina de apertura modal de otra ventana
		// Inhabilitamos la Ventana Actual
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setEnabled(false);
		// Y le damos paso a la siguiente Ventana, pasandole como contexto esta pestaña
		A2_Actores_Principal_GUI a2_Actores_Principal_GUI = new A2_Actores_Principal_GUI(this);
		a2_Actores_Principal_GUI.setVisible(true);
	}

	private void abrirVentanaPeliculas() {
		// Inhabilitamos la Ventana Actual
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setEnabled(false);
		// Y le damos paso a la siguiente Ventana, pasandole como contexto esta pestaña
		A3_Peliculas_Principal_GUI a3_Peliculas_Principal_GUI= new A3_Peliculas_Principal_GUI(this);
		a3_Peliculas_Principal_GUI.setVisible(true);

	}

	// METODO PARA NOTIFICAR MENSAJES AL USUARIO MEDIANTE UN JOPTIONPANEL
	public static void mostrarMensaje_A_Usuario(String mensaje, String cabecera) {

		JOptionPane.showMessageDialog(null, mensaje, cabecera, JOptionPane.ERROR_MESSAGE);

	}

	public static int mostrarMensaje_Si_No(String mensaje, String cabecera) {

		int resp = JOptionPane.showConfirmDialog(null, mensaje, cabecera, JOptionPane.YES_NO_OPTION);
		return resp;
	}

}
