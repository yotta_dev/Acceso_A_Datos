package a_interfaz_Grafica;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;

import b_clases_VideoClub.Actor;
import b_clases_VideoClub.Genero;
import b_clases_VideoClub.Pelicula;

public class C_Persistencia_MySQL implements C_Persistencia {

	public static Connection connection;

	// CONSTRUCTOR DE LA PERSISTENCIA MYSQL
	public C_Persistencia_MySQL(String server_ip, String usu, String pass, String baseDeDatos) throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://" + server_ip + "/" + baseDeDatos, usu,
					pass);

			System.out.println("CONECTADO");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void disconnect() throws SQLException {
		connection.close();
		System.out.println("Conexión con BD Cerrada");
	}

	@Override
	public ResultSet obtenerResultSet_De_Tabla(String tabla, String clave) {

		ResultSet resultSet = null;
		String sentenciaSQL;
		PreparedStatement preparedStatement;

		try {

			switch (tabla) {
			case "genero":
				if (clave == null) {
					sentenciaSQL = "SELECT * FROM " + tabla;// TODA LA TABLA
				} else {
					// SOLO LA CADENA INTRODUCIDA
					sentenciaSQL = "SELECT * FROM " + tabla + " WHERE descripcion LIKE '%" + clave + "%'";
				}
				preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
				resultSet = (ResultSet) preparedStatement.executeQuery();
				break;

			case "actor":
				if (clave == null) {
					sentenciaSQL = "SELECT * FROM " + tabla;// TODA LA TABLA
				} else {
					// SOLO LA CADENA INTRODUCIDA
					sentenciaSQL = "SELECT * FROM " + tabla + " WHERE nombre LIKE '%" + clave + "%'";
				}
				preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
				resultSet = (ResultSet) preparedStatement.executeQuery();
				break;
				
			case "pelicula":
				if (clave == null) {
					sentenciaSQL = "SELECT * FROM " + tabla;// TODA LA TABLA
				} else {
					// SOLO LA CADENA INTRODUCIDA
					sentenciaSQL = "SELECT * FROM " + tabla + " WHERE titulo LIKE '%" + clave + "%'";
				}
				preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
				resultSet = (ResultSet) preparedStatement.executeQuery();
				break;
				
			case "pertenece":
				//SELECT descripcion FROM genero WHERE idgenero IN(SELECT idgenero FROM pertenece WHERE idpelicula=1);
				if (clave==null) {
					sentenciaSQL = "SELECT descripcion FROM genero";// TODA LA TABLA
					preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
				}else {
					sentenciaSQL = "SELECT descripcion FROM genero WHERE idgenero IN(SELECT idgenero FROM "+tabla+" WHERE idpelicula=?)";// TODA LA TABLA
					preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
					preparedStatement.setString(1, clave);
				}
				
				resultSet = (ResultSet) preparedStatement.executeQuery();
				break;
				
			case "actua":
				//SELECT nombre FROM actor WHERE idactor IN(SELECT idactor FROM actua WHERE idpelicula=1);
				if (clave==null) {
					sentenciaSQL = "SELECT nombre FROM actor";// TODA LA TABLA
					preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
				}else {
					sentenciaSQL = "SELECT nombre FROM actor WHERE idactor IN(SELECT idactor FROM "+tabla+" WHERE idpelicula=?)";// TODA LA TABLA
					preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
					preparedStatement.setString(1, clave);
				}
				
				resultSet = (ResultSet) preparedStatement.executeQuery();
				break;
			default:
				break;
			}

			if (resultSet.next()) {// Existe
				return resultSet;
			} else {// No Existe
				return null;
			}
		} catch (SQLException exception) {
			A_Init_and_Config.mostrarMensaje_A_Usuario(exception.getMessage(), "Error BBDD");
			return null;
		}

	}

	@Override
	public void guardarObjeto(String tabla, Object object) throws Exception {

		String insert;
		PreparedStatement prepareStatement;

		switch (tabla) {
		case "genero":
			b_clases_VideoClub.Genero genero = (Genero) object;

			insert = "INSERT INTO " + tabla + " VALUES (?,?)";

			prepareStatement = (PreparedStatement) connection.prepareStatement(insert);

			prepareStatement.setString(1, null);
			prepareStatement.setString(2, genero.getDescripcion());

			prepareStatement.executeUpdate();
			break;

		case "actor":
			b_clases_VideoClub.Actor actor = (Actor) object;

			insert = "INSERT INTO " + tabla + " VALUES (?,?,?)";

			prepareStatement = (PreparedStatement) connection.prepareStatement(insert);

			prepareStatement.setString(1, null);
			prepareStatement.setString(2, actor.getNombre());
			prepareStatement.setDate(3, (Date) actor.getFechanac());

			prepareStatement.executeUpdate();
			break;

		case "pelicula":
			b_clases_VideoClub.Pelicula pelicula = (Pelicula) object;

			insert = "INSERT INTO " + tabla + " VALUES (?,?,?,?)";

			prepareStatement = (PreparedStatement) connection.prepareStatement(insert);

			prepareStatement.setString(1, null);
			prepareStatement.setString(2, pelicula.getTitulo());
			prepareStatement.setString(3, pelicula.getSinopsis());
			prepareStatement.setInt(4, pelicula.getPreciodia());

			prepareStatement.executeUpdate();
			break;
			
		case "pertenece":			
			genero = (Genero) object;
			
			//INSERT INTO pertenece VALUES ((SELECT idgenero FROM genero WHERE descripcion = 'Suspense'),1);
			insert = "INSERT INTO " + tabla + " VALUES ((SELECT idgenero FROM genero WHERE descripcion = ?),?)";

			prepareStatement = (PreparedStatement) connection.prepareStatement(insert);

			prepareStatement.setString(1, genero.getDescripcion());
			prepareStatement.setInt(2, genero.getIdgenero());//Aqui va el valor del idPelicula "Encubierto" - me he aprovechado del Contructor
	
			prepareStatement.executeUpdate();
			break;
			
		case "actua":			
			actor = (Actor) object;
			
			//INSERT INTO actua VALUES ((SELECT idactor FROM actor WHERE nombre = 'Brad Pitt'),3);
			insert = "INSERT INTO " + tabla + " VALUES ((SELECT idactor FROM actor WHERE nombre = ?),?)";

			prepareStatement = (PreparedStatement) connection.prepareStatement(insert);

			prepareStatement.setString(1, actor.getNombre());
			prepareStatement.setInt(2, actor.getIdactor());//Aqui va el valor del idPelicula "Encubierto" - me he aprovechado del Contructor del Actor
	
			prepareStatement.executeUpdate();
			break;
		default:
			break;
		}

	}

	@Override
	public void modificarObjeto(String tabla, Object object) throws Exception {
		String update;
		PreparedStatement prepareStatement;

		switch (tabla) {
		case "genero":
			b_clases_VideoClub.Genero genero = (Genero) object;

			update = "UPDATE " + tabla + " SET descripcion=? WHERE idgenero=?";

			prepareStatement = (PreparedStatement) connection.prepareStatement(update);

			prepareStatement.setString(1, genero.getDescripcion());
			prepareStatement.setInt(2, genero.getIdgenero());

			prepareStatement.executeUpdate();
			break;

		case "actor":
			b_clases_VideoClub.Actor actor = (Actor) object;

			update = "UPDATE " + tabla + " SET nombre=?,fechaNac=? WHERE idactor=?";

			prepareStatement = (PreparedStatement) connection.prepareStatement(update);

			prepareStatement.setString(1, actor.getNombre());
			prepareStatement.setDate(2, (Date) actor.getFechanac());
			prepareStatement.setInt(3, actor.getIdactor());

			prepareStatement.executeUpdate();
			break;
			
		case "pelicula":
			b_clases_VideoClub.Pelicula pelicula = (Pelicula) object;

			update = "UPDATE " + tabla + " SET titulo=?,sinopsis=?,preciodia=? WHERE idpelicula=?";

			prepareStatement = (PreparedStatement) connection.prepareStatement(update);

			prepareStatement.setString(1, pelicula.getTitulo());
			prepareStatement.setString(2, pelicula.getSinopsis());
			prepareStatement.setInt(3, pelicula.getPreciodia());
			prepareStatement.setInt(4, pelicula.getIdpelicula());

			prepareStatement.executeUpdate();
			break;
			
		default:
			break;
		}

	}

	@Override
	public void borrarObjeto(String tabla, String clave) throws Exception {
		String delete;
		PreparedStatement prepareStatement;

		switch (tabla) {
		case "genero":

			delete = "DELETE FROM " + tabla + " WHERE idgenero = ?";

			prepareStatement = (PreparedStatement) connection.prepareStatement(delete);

			prepareStatement.setString(1, clave);

			prepareStatement.executeUpdate();
			break;

		case "actor":

			delete = "DELETE FROM " + tabla + " WHERE idactor = ?";

			prepareStatement = (PreparedStatement) connection.prepareStatement(delete);

			prepareStatement.setString(1, clave);

			prepareStatement.executeUpdate();
			break;
			
		case "pelicula":

			delete = "DELETE FROM " + tabla + " WHERE idpelicula = ?";

			prepareStatement = (PreparedStatement) connection.prepareStatement(delete);

			prepareStatement.setString(1, clave);

			prepareStatement.executeUpdate();
			break;
			
		case "pertenece":

			delete = "DELETE FROM " + tabla + " WHERE idpelicula = ?";

			prepareStatement = (PreparedStatement) connection.prepareStatement(delete);

			prepareStatement.setString(1, clave);

			prepareStatement.executeUpdate();
			break;
			
		case "actua":

			delete = "DELETE FROM " + tabla + " WHERE idpelicula = ?";

			prepareStatement = (PreparedStatement) connection.prepareStatement(delete);

			prepareStatement.setString(1, clave);

			prepareStatement.executeUpdate();
			break;
			
		default:
			break;
		}

	}

	@Override
	public Boolean consultar_Existencia_Objeto(String tabla, String clave) throws Exception {

		ResultSet resultSet = null;
		String sentenciaSQL;
		PreparedStatement preparedStatement;
		try {
			switch (tabla) {
			case "genero":
				sentenciaSQL = "SELECT * FROM " + tabla + " WHERE descripcion LIKE '%" + clave + "%'";
				preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
				resultSet = (ResultSet) preparedStatement.executeQuery();
				break;

			case "actor":
				sentenciaSQL = "SELECT * FROM " + tabla + " WHERE nombre LIKE '%" + clave + "%'";
				preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
				resultSet = (ResultSet) preparedStatement.executeQuery();
				break;
				
			case "pelicula":
				sentenciaSQL = "SELECT * FROM " + tabla + " WHERE titulo LIKE '%" + clave + "%'";
				preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
				resultSet = (ResultSet) preparedStatement.executeQuery();
				break;

			default:
				break;
			}

			if (resultSet.next()) {// Existe
				return true;
			} else {// No Existe
				return false;
			}

		} catch (SQLException exception) {

			A_Init_and_Config.mostrarMensaje_A_Usuario(exception.getMessage(), "Error BBDD");

		}
		return false;
	}

}
