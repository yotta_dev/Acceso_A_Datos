package a_interfaz_Grafica;

import com.mysql.jdbc.ResultSet;

public interface C_Persistencia {

	void disconnect() throws Exception;

	ResultSet obtenerResultSet_De_Tabla(String tabla,String clave);

	void guardarObjeto(String tabla, Object object) throws Exception;// inserta 
	
	void modificarObjeto(String tabla, Object object) throws Exception;// modifica

	void borrarObjeto(String tabla, String clave) throws Exception; //borra

	Boolean consultar_Existencia_Objeto(String tabla, String clave) throws Exception; //consulta

}
