package a_interfaz_Grafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import b_clases_VideoClub.Pelicula;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class A3_Peliculas_Principal_GUI extends JFrame {

	private JPanel contentPane;
	private JLabel label_Titulo;

	// JTEXFIELD PUBLICOS PARA PODER HACER SETTEXT CUANDO LA VENTANA DE
	// BUSQUEDA DEVUELVA UN RESULTADO
	public static JTextField jTextfield_id_Pelicula;
	public static JTextField jTextField_Titulo;
	public static JTextArea textArea_Sinopsis;
	public static JTextField jTextField_PrecioDia;
	// Boton Guardar para ser activado o no si procede
	public static JButton button_Guardar;
	public static JButton button_Eliminar;
	public static JButton button_Actores;
	public static JButton button_Generos;

	public C_Persistencia_MySQL c_Persistencia_MySQL = A_Init_and_Config.c_Persistencia_MySQL;

	// Objecto Frame Padre para realizar operaciones Modales
	static javax.swing.JFrame padre;

	public static String titulo_Original;
	public static String sinopsis_Original;
	public static String precioDia_Original;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// Visualizacion del Frame
					A3_Peliculas_Principal_GUI frame = new A3_Peliculas_Principal_GUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// CONTROL DE LO QUE REALIZA EL FRAME TRAS LA PULSACION DEL BOTON SALIR
	// SOBRE EL FRAME
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			// HABILITA EL FRAME DEL PADRE
			padre.setEnabled(true);
			padre.toFront();
			padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		}
	}

	public A3_Peliculas_Principal_GUI(javax.swing.JFrame padre) {
		setResizable(false);
		// REALIZAMOS LAS ACCIONES PERTINENTES PARA ESTAR LIGADO AL FRAME PADRE
		A3_Peliculas_Principal_GUI.padre = padre;
		this.toFront();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setBackground(UIManager.getColor("Button.background"));
		setTitle("Peliculas");
		setBounds(100, 100, 428, 371);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("ComboBox.selectionBackground"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label_Pelicula = new JLabel("id_Pelicula :");
		label_Pelicula.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_Pelicula.setBounds(55, 36, 94, 23);
		contentPane.add(label_Pelicula);

		jTextfield_id_Pelicula = new JTextField();
		jTextfield_id_Pelicula.setFont(new Font("Tahoma", Font.PLAIN, 15));
		jTextfield_id_Pelicula.setEnabled(false);
		jTextfield_id_Pelicula.setBounds(162, 33, 94, 30);
		contentPane.add(jTextfield_id_Pelicula);
		jTextfield_id_Pelicula.setColumns(10);

		label_Titulo = new JLabel("Titulo :");
		label_Titulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_Titulo.setBounds(54, 83, 95, 15);
		contentPane.add(label_Titulo);

		jTextField_Titulo = new JTextField();
		jTextField_Titulo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		jTextField_Titulo.setBounds(163, 76, 165, 30);
		contentPane.add(jTextField_Titulo);
		jTextField_Titulo.setColumns(10);

		JLabel label_Sinopsis = new JLabel("Sinopsis :");
		label_Sinopsis.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_Sinopsis.setBounds(55, 124, 95, 15);
		contentPane.add(label_Sinopsis);

		button_Guardar = new JButton("Guardar");
		button_Guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				guardarPelicula();
			}
		});

		textArea_Sinopsis = new JTextArea();
		textArea_Sinopsis.setEnabled(false);
		textArea_Sinopsis.setBounds(162, 117, 166, 53);
		contentPane.add(textArea_Sinopsis);

		JLabel label_PrecioDia = new JLabel("Precio Dia :");
		label_PrecioDia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_PrecioDia.setBounds(55, 191, 95, 15);
		contentPane.add(label_PrecioDia);

		jTextField_PrecioDia = new JTextField();
		jTextField_PrecioDia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		jTextField_PrecioDia.setEnabled(false);
		jTextField_PrecioDia.setColumns(10);
		jTextField_PrecioDia.setBounds(162, 184, 167, 30);
		contentPane.add(jTextField_PrecioDia);
		button_Guardar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_Guardar.setEnabled(false);
		button_Guardar.setBounds(84, 279, 111, 30);
		contentPane.add(button_Guardar);

		JButton buttonCancelar = new JButton("Cancelar");
		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cancelar();

			}
		});
		buttonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		buttonCancelar.setBounds(233, 279, 111, 30);
		contentPane.add(buttonCancelar);

		JButton button_Buscar = new JButton("");
		button_Buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Posteriormente creo que aqui tengo que implementar que la cadena Descripcion
				// este validada de cierta manera
				try {
					abrirVentanaBusquedaPeliculas();
				} catch (Exception e) {
					A_Init_and_Config.mostrarMensaje_A_Usuario("test", "Exception Ventana Busqueda Actores");
				}

			}
		});
		button_Buscar.setBorderPainted(false);
		button_Buscar.setBorder(null);
		button_Buscar.setIcon(new ImageIcon(
				"C:\\Users\\TheSlayeOne1\\Desktop\\Repositorios\\Acceso_A_Datos\\Practica_VideoClub\\resources\\imgs\\loupe_78347.png"));
		button_Buscar.setBounds(338, 76, 33, 30);
		contentPane.add(button_Buscar);

		button_Eliminar = new JButton("Eliminar");
		button_Eliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				eliminarPelicula();
			}
		});
		button_Eliminar.setVisible(false);
		button_Eliminar.setFont(new Font("Dialog", Font.PLAIN, 15));
		button_Eliminar.setBounds(268, 33, 103, 30);
		contentPane.add(button_Eliminar);

		button_Generos = new JButton("Generos");
		button_Generos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				asociar_quitar_generos();
			}
		});
		button_Generos.setEnabled(false);
		button_Generos.setBounds(99, 237, 89, 23);
		contentPane.add(button_Generos);

		button_Actores = new JButton("Actores");
		button_Actores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				asociar_quitar_actores();
			}
		});
		button_Actores.setEnabled(false);
		button_Actores.setBounds(245, 237, 89, 23);
		contentPane.add(button_Actores);

		this.setLocationRelativeTo(padre);
	}

	// INICIO VARIABLES/OBJETOS Y METODOS CREADOS POR EL DESARROLADOR
	protected void guardarPelicula() {
		b_clases_VideoClub.Pelicula pelicula;
		String idPelicula = jTextfield_id_Pelicula.getText();
		String tituloIntroducido = jTextField_Titulo.getText();
		String sinopsisIntroducida = textArea_Sinopsis.getText();
		String precioDiaIntroducido = jTextField_PrecioDia.getText();

		switch (button_Guardar.getText()) {
		case "Guardar":
			if (tituloIntroducido.length() == 0 || sinopsisIntroducida.length() == 0
					|| precioDiaIntroducido.length() == 0) {
				A_Init_and_Config.mostrarMensaje_A_Usuario(
						"�Debe rellenar todos los campos!\nTitulo - Sinopsis - Precio", "Error en Campos");
				jTextField_Titulo.grabFocus();
			} else {

				try {// PRIMERO COMPROBAMOS QUE EL GENERO INTRODUCIDO NO EXISTA YA ALMACENADO EN LA
						// TABLA GENEROS
					if (!c_Persistencia_MySQL.consultar_Existencia_Objeto("pelicula", jTextField_Titulo.getText())) {
						// SINO EXISTE PROCEDEMOS A GUARDAR EL GENERO CON LA DESCRIPCION INTRODUCIDA
						pelicula = new Pelicula(tituloIntroducido, sinopsisIntroducida,
								Integer.valueOf(precioDiaIntroducido));
						c_Persistencia_MySQL.guardarObjeto("pelicula", pelicula);
						A_Init_and_Config.mostrarMensaje_A_Usuario("Pelicula guardada correctamente", "Guardada");
						jTextField_Titulo.setText("");
						jTextField_Titulo.grabFocus();
						textArea_Sinopsis.setText("");
						textArea_Sinopsis.setEnabled(false);
						jTextField_PrecioDia.setText("");
						jTextField_PrecioDia.setEnabled(false);
						button_Guardar.setEnabled(false);

					} else {
						// CANCELAMOS LA OPERACION PORQUE LA DESCRIPCION INTRODUCIDA YA EXISTE
						A_Init_and_Config.mostrarMensaje_A_Usuario("El Titulo introducido ya Existe",
								"Coincidencia Encontrada");
						jTextField_Titulo.grabFocus();
						jTextField_Titulo.setText("");
					}
				} catch (Exception e) {

					A_Init_and_Config.mostrarMensaje_A_Usuario("El precio debe ser una Cifra", "Error en Precio");
					jTextField_PrecioDia.grabFocus();
				}
			}
			break;

		case "Modificar":

			if (!tituloIntroducido.equals(titulo_Original) || !sinopsisIntroducida.equals(sinopsis_Original)
					|| !precioDiaIntroducido.equals(precioDia_Original)) {

				try {
					pelicula = new Pelicula(Integer.valueOf(idPelicula), tituloIntroducido, sinopsisIntroducida,
							Integer.valueOf(precioDiaIntroducido));
					c_Persistencia_MySQL.modificarObjeto("pelicula", pelicula);
					A_Init_and_Config.mostrarMensaje_A_Usuario("Pelicula Modificada Correctamente", "Modificada");
					jTextfield_id_Pelicula.setText("");
					jTextField_Titulo.setText("");
					jTextField_Titulo.grabFocus();
					textArea_Sinopsis.setEnabled(false);
					textArea_Sinopsis.setText("");
					jTextField_PrecioDia.setEnabled(false);
					jTextField_PrecioDia.setText("");

					button_Guardar.setEnabled(false);
					button_Guardar.setText("Guardar");
					button_Eliminar.setVisible(false);

				} catch (Exception e) {
					A_Init_and_Config.mostrarMensaje_A_Usuario("El Precio debe ser una Cifra", "Error en Precio");
					jTextField_PrecioDia.grabFocus();
				}

			} else {// MODIFICAMOS LA DESCRIPCION Y REANUDAMOS LA OPERACION

				A_Init_and_Config.mostrarMensaje_A_Usuario(
						"No se ha realizado ninguna modificaci�n\nDebe modificar algo", "Modificar");
				jTextField_Titulo.grabFocus();
			}

			break;
		}
	}

	protected void eliminarPelicula() {

		if (A_Init_and_Config.mostrarMensaje_Si_No("�Esta seguro de Eliminar la Pelicula?",
				"�Alerta!") == JOptionPane.YES_NO_OPTION) {
			// Ha seleccionado SI asi que borramos
			try {
				c_Persistencia_MySQL.borrarObjeto("pelicula", jTextfield_id_Pelicula.getText());
				A_Init_and_Config.mostrarMensaje_A_Usuario("Pelicula Eliminada Correctamente", "Eliminada");
				cancelar();
			} catch (Exception e) {
				jTextField_Titulo.grabFocus();
				e.printStackTrace();
			}
		} else {
			// Ha seleccionado NO asi que volvemos al estado en el que nos encontrabamos
			jTextField_Titulo.grabFocus();
		}
	}

	protected void cancelar() {
		jTextfield_id_Pelicula.setText("");

		jTextField_Titulo.setText("");
		jTextField_Titulo.grabFocus();

		textArea_Sinopsis.setEnabled(false);
		textArea_Sinopsis.setText("");

		jTextField_PrecioDia.setEnabled(false);
		jTextField_PrecioDia.setText("");

		button_Guardar.setText("Guardar");
		button_Guardar.setEnabled(false);

		button_Eliminar.setVisible(false);
		button_Eliminar.setEnabled(false);

		button_Generos.setEnabled(false);
		button_Actores.setEnabled(false);
	}

	private void abrirVentanaBusquedaPeliculas() throws Exception {

		if (c_Persistencia_MySQL.consultar_Existencia_Objeto("pelicula", jTextfield_id_Pelicula.getText().trim())
				|| button_Eliminar.isEnabled()) {
			// Rutina de apertura modal de otra ventana
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			this.setEnabled(false);

			A3_Peliculas_Secundaria_GUI a3_Peliculas_Secundaria_GUI = new A3_Peliculas_Secundaria_GUI(this);
			a3_Peliculas_Secundaria_GUI.setVisible(true);

		} else {
			int resp = A_Init_and_Config.mostrarMensaje_Si_No(
					"No hay Peliculas con el Titulo " + jTextField_Titulo.getText() + "\n �Quieres Guardarla?",
					"Nombre no Encontrado");
			if (resp == JOptionPane.YES_OPTION) {
				b_clases_VideoClub.Pelicula pelicula;
				try {
					pelicula = new Pelicula(jTextField_Titulo.getText(), textArea_Sinopsis.getText(),
							Integer.valueOf(jTextField_PrecioDia.getText()));
					c_Persistencia_MySQL.guardarObjeto("pelicula", pelicula);
					A_Init_and_Config.mostrarMensaje_A_Usuario("Pelicula guardada correctamente", "Guardada");
					jTextField_Titulo.setText("");
					jTextField_Titulo.grabFocus();
					textArea_Sinopsis.setText("");
					textArea_Sinopsis.setEnabled(false);
					jTextField_PrecioDia.setText("");
					jTextField_PrecioDia.setEnabled(false);
					button_Guardar.setEnabled(false);
				} catch (Exception e) {

					A_Init_and_Config.mostrarMensaje_A_Usuario("El Precio debe ser una Cifra", "Error en Precio");
					jTextField_PrecioDia.grabFocus();
				}

			} else {
				cancelar();
			}
		}

	}

	protected void asociar_quitar_generos() {

		// Rutina de apertura modal de otra ventana
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setEnabled(false);
		A3_Peliculas_Generos_GUI a3_Peliculas_Generos_GUI = new A3_Peliculas_Generos_GUI(this);
		a3_Peliculas_Generos_GUI.setVisible(true);
	}

	protected void asociar_quitar_actores() {
		// Rutina de apertura modal de otra ventana
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setEnabled(false);
		A3_Peliculas_Actores_GUI a3_Peliculas_Actores_GUI = new A3_Peliculas_Actores_GUI(this);
		a3_Peliculas_Actores_GUI.setVisible(true);

	}
}
