package a_interfaz_Grafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;

@SuppressWarnings("serial")
public class A2_Actores_Secundaria_GUI extends JFrame {

	private JPanel contentPane;
	public JTable jtableActores;

	public Connection connection;

	static javax.swing.JFrame padre;

	public C_Persistencia_MySQL c_Persistencia_MySQL = A_Init_and_Config.c_Persistencia_MySQL;

	public JLabel jlabelTitulo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					A2_Actores_Secundaria_GUI frame = new A2_Actores_Secundaria_GUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// CONTROL DE LO QUE REALIZA LA APLICACION TRAS LA PULSACION DEL BOTON SALIR
	// SOBRE EL FRAME
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			// HABILITA EL FRAME DEL PADRE
			padre.setEnabled(true);
			padre.toFront();
			padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		}
	}

	// CONSTRUCTORES DE LA CLASE - Creamos Clase y Lanzamos FRAME

	// CONSTRUCTOR CON FRAME PADRE
	public A2_Actores_Secundaria_GUI(javax.swing.JFrame padre) {
		setResizable(false);

		// REALIZAMOS LAS ACCIONES PERTINENTES PARA ESTAR LIGADO AL FRAME PADRE
		A2_Actores_Secundaria_GUI.padre = padre;
		this.toFront();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setTitle("Listado de Actores");
		setBounds(100, 100, 469, 397);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("Button.select"));
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton buttonNuevoActor = new JButton("Nuevo");
		buttonNuevoActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				volver();
				A2_Actores_Principal_GUI.button_Guardar.setEnabled(true);
				A2_Actores_Principal_GUI.button_Guardar.setText("Guardar");
				A2_Actores_Principal_GUI.button_Eliminar.setVisible(false);
				
				A2_Actores_Principal_GUI.jTextfield_id_Actor.setText(null);
				A2_Actores_Principal_GUI.jTextField_Nombre.setText(null);
				A2_Actores_Principal_GUI.jTextField_Nombre.grabFocus();
				A2_Actores_Principal_GUI.jTextField_FechaNac.setText(null);
				A2_Actores_Principal_GUI.jTextField_FechaNac.setEnabled(true);
			}
		});
		buttonNuevoActor.setBounds(100, 317, 117, 25);
		contentPane.add(buttonNuevoActor);

		JButton button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				volver();
			}
		});
		button_Cancelar.setBounds(255, 317, 117, 25);
		contentPane.add(button_Cancelar);

		// Tras crear los controles de la Ventana Secundaria de Busqueda de Genero,
		// procedemos
		// A cargar en la JTABLE de esta nueva ventana toda la informacion referente a
		// los Generos
		// filtrada por la busqueda introducida, sino genera ningun resultado, se
		// muestran todos las
		// descripciones almacenadas

		jtableActores = new JTable();
		jtableActores.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		jtableActores.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent mouseEvent) {
				if (mouseEvent.getClickCount() > 1) {
					int row = jtableActores.getSelectedRow();

					String id_actor = jtableActores.getValueAt(row, 0).toString();
					String nombre = jtableActores.getValueAt(row, 1).toString();
					String fechanac = jtableActores.getValueAt(row, 2).toString();

					// Devolvemos datos de la seleccion a la anterior Ventana
					A2_Actores_Principal_GUI.jTextfield_id_Actor.setText(id_actor);
					A2_Actores_Principal_GUI.jTextField_Nombre.setText(nombre);
					A2_Actores_Principal_GUI.jTextField_FechaNac.setEnabled(true);
					A2_Actores_Principal_GUI.jTextField_FechaNac.setText(fechanac);					
					A2_Actores_Principal_GUI.nombre_Original = nombre;
					// Modifico el texto del boton Guardar para que modifique
					A2_Actores_Principal_GUI.button_Guardar.setEnabled(true);
					A2_Actores_Principal_GUI.button_Guardar.setText("Modificar");
					// Y habilitamos la opcion de Eliminar si lo que se desea es eliminar el Genero
					A2_Actores_Principal_GUI.button_Eliminar.setVisible(true);
					A2_Actores_Principal_GUI.button_Eliminar.setEnabled(true);

					volver();

				}
			}
		});
		
		//COMPONENTE JTABLE
		jtableActores.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		jtableActores.setModel(
				new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"id_Actor", "Nombre", "Fecha_Nac"
			}
		));
		jtableActores.setBounds(43, 96, 380, 198);
		contentPane.add(jtableActores);			

		DefaultTableModel modelo = new DefaultTableModel();

		try {
			// modelo.addColumn("Id_Género");
			// modelo.addColumn("Descripción");
			modelo.setColumnIdentifiers(new String[] { "Id_Actor", "Nombre","Fecha_Nac"});
			
			ResultSet resultSet = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("actor",
					A2_Actores_Principal_GUI.jTextField_Nombre.getText());

			Object[] fila = new Object[3]; // numeroColumnas es el número de columnas de la tabla y del ResultSet

			resultSet.beforeFirst();

			while (resultSet.next()) {

				for (int i = 0; i < 3; i++) {
					fila[i] = resultSet.getObject(i + 1);
				}
				modelo.addRow(fila);
			}

			jtableActores.setModel(modelo);

			jlabelTitulo = new JLabel("Actores que contienen ");
			jlabelTitulo.setFont(new Font("Tahoma", Font.BOLD, 15));
			jlabelTitulo.setHorizontalAlignment(SwingConstants.CENTER);
			jlabelTitulo.setBounds(100, 23, 272, 32);
			contentPane.add(jlabelTitulo);

			JLabel jlabelSubTitutlo = new JLabel("Para modificar un Actor/Actriz haga doble click sobre \u00E9l:");
			jlabelSubTitutlo.setHorizontalAlignment(SwingConstants.CENTER);
			jlabelSubTitutlo.setFont(new Font("Tahoma", Font.PLAIN, 10));
			jlabelSubTitutlo.setBounds(100, 60, 272, 25);
			contentPane.add(jlabelSubTitutlo);

		} catch (SQLException e) {
			A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "SQL Exception");
			e.printStackTrace();
		}

		this.setLocationRelativeTo(padre);
		
		String nombre = A2_Actores_Principal_GUI.jTextField_Nombre.getText();

		if (nombre.length() == 0) {
			jlabelTitulo.setText("Listado Completo de Actores");
		} else {
			 jlabelTitulo.setText("Actores que contienen : " +
					 A2_Actores_Principal_GUI.jTextField_Nombre.getText());
		}
	}

	public void volver() {

		this.dispose();
		padre.setEnabled(true);
		padre.toFront();
		padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);	
		A2_Actores_Principal_GUI.jTextField_Nombre.grabFocus();

	}
}
