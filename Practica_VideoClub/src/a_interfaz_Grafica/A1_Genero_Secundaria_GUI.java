package a_interfaz_Grafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;

@SuppressWarnings("serial")
public class A1_Genero_Secundaria_GUI extends JFrame {

	private JPanel contentPane;
	public JTable jtableGeneros;

	public Connection connection;

	static javax.swing.JFrame padre;

	public C_Persistencia_MySQL c_Persistencia_MySQL = A_Init_and_Config.c_Persistencia_MySQL;

	public JLabel jlabelTitulo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					A1_Genero_Secundaria_GUI frame = new A1_Genero_Secundaria_GUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// CONTROL DE LO QUE REALIZA LA APLICACION TRAS LA PULSACION DEL BOTON SALIR
	// SOBRE EL FRAME
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			// HABILITA EL FRAME DEL PADRE
			padre.setEnabled(true);
			padre.toFront();
			padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		}
	}

	// CONSTRUCTORES DE LA CLASE - Creamos Clase y Lanzamos FRAME

	// CONSTRUCTOR CON FRAME PADRE
	public A1_Genero_Secundaria_GUI(javax.swing.JFrame padre) {
		setResizable(false);

		// REALIZAMOS LAS ACCIONES PERTINENTES PARA ESTAR LIGADO AL FRAME PADRE
		A1_Genero_Secundaria_GUI.padre = padre;
		this.toFront();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setTitle("Listado de Generos");
		setBounds(100, 100, 362, 409);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("Button.select"));
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton buttonNuevoGenero = new JButton("Nuevo");
		buttonNuevoGenero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				volver();
				A1_Genero_Principal_GUI.button_Guardar.setEnabled(true);
				A1_Genero_Principal_GUI.button_Guardar.setText("Guardar");
				A1_Genero_Principal_GUI.button_Eliminar.setVisible(false);
				
				A1_Genero_Principal_GUI.jTextfield_id_Genero.setText(null);
				A1_Genero_Principal_GUI.jTextField_Descripcion.setText(null);
			}
		});
		buttonNuevoGenero.setBounds(42, 317, 117, 25);
		contentPane.add(buttonNuevoGenero);

		JButton button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				volver();
			}
		});
		button_Cancelar.setBounds(197, 317, 117, 25);
		contentPane.add(button_Cancelar);

		// Tras crear los controles de la Ventana Secundaria de Busqueda de Genero,
		// procedemos
		// A cargar en la JTABLE de esta nueva ventana toda la informacion referente a
		// los Generos
		// filtrada por la busqueda introducida, sino genera ningun resultado, se
		// muestran todos las
		// descripciones almacenadas

		jtableGeneros = new JTable();
		jtableGeneros.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		jtableGeneros.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent mouseEvent) {
				if (mouseEvent.getClickCount() > 1) {
					int row = jtableGeneros.getSelectedRow();

					String id_genero = jtableGeneros.getValueAt(row, 0).toString();
					String descripcion = jtableGeneros.getValueAt(row, 1).toString();

					// Devolvemos datos de la seleccion a la anterior Ventana
					A1_Genero_Principal_GUI.jTextfield_id_Genero.setText(id_genero);
					A1_Genero_Principal_GUI.jTextField_Descripcion.setText(descripcion);
					A1_Genero_Principal_GUI.descripcion_Original = descripcion;
					// Modifico el texto del boton Guardar para que modifique
					A1_Genero_Principal_GUI.button_Guardar.setEnabled(true);
					A1_Genero_Principal_GUI.button_Guardar.setText("Modificar");
					// Y habilitamos la opcion de Eliminar si lo que se desea es eliminar el Genero
					A1_Genero_Principal_GUI.button_Eliminar.setVisible(true);
					A1_Genero_Principal_GUI.button_Eliminar.setEnabled(true);

					volver();

				}
			}
		});
		
		//COMPONENTE JTABLE
		jtableGeneros.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		jtableGeneros.setModel(
				new DefaultTableModel(new Object[][] { { null, null }, }, new String[] { "Id_Genero", "Descripcion" }));
		jtableGeneros.setBounds(42, 91, 272, 198);
		contentPane.add(jtableGeneros);			

		DefaultTableModel modelo = new DefaultTableModel();

		try {
			// modelo.addColumn("Id_Género");
			// modelo.addColumn("Descripción");
			modelo.setColumnIdentifiers(new String[] { "Id_Genero", "Descripcion" });
			
			ResultSet resultSet = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("genero",
					A1_Genero_Principal_GUI.jTextField_Descripcion.getText());

			Object[] fila = new Object[2]; // numeroColumnas es el número de columnas de la tabla y del ResultSet

			resultSet.beforeFirst();

			while (resultSet.next()) {

				for (int i = 0; i < 2; i++) {
					fila[i] = resultSet.getObject(i + 1);
				}
				modelo.addRow(fila);
			}

			jtableGeneros.setModel(modelo);

			jlabelTitulo = new JLabel("G\u00E9neros que contienen ");
			jlabelTitulo.setFont(new Font("Tahoma", Font.BOLD, 15));
			jlabelTitulo.setHorizontalAlignment(SwingConstants.CENTER);
			jlabelTitulo.setBounds(42, 28, 272, 32);
			contentPane.add(jlabelTitulo);

			JLabel jlabelSubTitutlo = new JLabel("Para modificar un g\u00E9nero haga doble click sobre \u00E9l:");
			jlabelSubTitutlo.setHorizontalAlignment(SwingConstants.CENTER);
			jlabelSubTitutlo.setFont(new Font("Tahoma", Font.PLAIN, 10));
			jlabelSubTitutlo.setBounds(42, 65, 272, 25);
			contentPane.add(jlabelSubTitutlo);

		} catch (SQLException e) {
			A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "SQL Exception");
			e.printStackTrace();
		}

		this.setLocationRelativeTo(padre);
		
		String descripcion = A1_Genero_Principal_GUI.jTextField_Descripcion.getText();

		if (descripcion.length() == 0) {
			jlabelTitulo.setText("Listado Completo de Generos");
		} else {
			 jlabelTitulo.setText("Generos que contienen : " +
			 A1_Genero_Principal_GUI.jTextField_Descripcion.getText());
		}
	}

	public void volver() {

		this.dispose();
		padre.setEnabled(true);
		padre.toFront();
		padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		A1_Genero_Principal_GUI.jTextField_Descripcion.grabFocus();

	}
}
