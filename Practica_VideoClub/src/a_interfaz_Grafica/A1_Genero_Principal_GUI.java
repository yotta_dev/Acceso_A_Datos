package a_interfaz_Grafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.hibernate.type.YesNoType;

import com.mysql.jdbc.ResultSet;

import b_clases_VideoClub.Genero;

@SuppressWarnings("serial")
public class A1_Genero_Principal_GUI extends JFrame {

	private JPanel contentPane;
	private JLabel lblDescripcion;

	// JTEXFIELD PUBLICOS PARA PODER HACER SETTEXT CUANDO LA VENTANA DE
	// BUSQUEDA DEVUELVA UN RESULTADO
	public static JTextField jTextfield_id_Genero;
	public static JTextField jTextField_Descripcion;

	public C_Persistencia_MySQL c_Persistencia_MySQL = A_Init_and_Config.c_Persistencia_MySQL;

	// Objecto Frame Padre para realizar operaciones Modales
	static javax.swing.JFrame padre;

	// Boton Guardar para ser activado o no si procede
	public static JButton button_Guardar;
	public static JButton button_Eliminar;

	public static String descripcion_Original;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// Visualizacion del Frame
					A1_Genero_Principal_GUI frame = new A1_Genero_Principal_GUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// CONTROL DE LO QUE REALIZA EL FRAME TRAS LA PULSACION DEL BOTON SALIR
	// SOBRE EL FRAME
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			// HABILITA EL FRAME DEL PADRE
			padre.setEnabled(true);
			padre.toFront();
			padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		}
	}

	public A1_Genero_Principal_GUI(javax.swing.JFrame padre) {
		setResizable(false);
		// REALIZAMOS LAS ACCIONES PERTINENTES PARA ESTAR LIGADO AL FRAME PADRE
		A1_Genero_Principal_GUI.padre = padre;
		this.toFront();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setBackground(UIManager.getColor("Button.background"));
		setTitle("Generos");
		setBounds(100, 100, 428, 222);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("ComboBox.selectionBackground"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		jTextfield_id_Genero = new JTextField();
		jTextfield_id_Genero.setFont(new Font("Tahoma", Font.PLAIN, 15));
		jTextfield_id_Genero.setEnabled(false);
		jTextfield_id_Genero.setBounds(162, 33, 94, 30);
		contentPane.add(jTextfield_id_Genero);
		jTextfield_id_Genero.setColumns(10);

		JLabel lblIdgenero = new JLabel("id_Genero :");
		lblIdgenero.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblIdgenero.setBounds(55, 36, 94, 23);
		contentPane.add(lblIdgenero);

		lblDescripcion = new JLabel("Descripcion :");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDescripcion.setBounds(54, 83, 95, 15);
		contentPane.add(lblDescripcion);

		jTextField_Descripcion = new JTextField();
		jTextField_Descripcion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		jTextField_Descripcion.setBounds(163, 76, 165, 30);
		contentPane.add(jTextField_Descripcion);
		jTextField_Descripcion.setColumns(10);

		button_Guardar = new JButton("Guardar");
		button_Guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				guardarGenero();
			}
		});
		button_Guardar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_Guardar.setEnabled(false);
		button_Guardar.setBounds(83, 136, 111, 30);
		contentPane.add(button_Guardar);

		JButton buttonCancelar = new JButton("Cancelar");
		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cancelar();

			}
		});
		buttonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		buttonCancelar.setBounds(232, 136, 111, 30);
		contentPane.add(buttonCancelar);

		JButton button_Buscar = new JButton("");
		button_Buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Posteriormente creo que aqui tengo que implementar que la cadena Descripcion
				// este validada de cierta manera
				abrirVentanaBusquedaGenero();

			}
		});
		button_Buscar.setBorderPainted(false);
		button_Buscar.setBorder(null);
		button_Buscar.setIcon(new ImageIcon(
				"C:\\Users\\TheSlayeOne1\\Desktop\\Repositorios\\Acceso_A_Datos\\Practica_VideoClub\\resources\\imgs\\loupe_78347.png"));
		button_Buscar.setBounds(338, 76, 33, 30);
		contentPane.add(button_Buscar);

		button_Eliminar = new JButton("Eliminar");
		button_Eliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				eliminarGenero();
			}
		});
		button_Eliminar.setVisible(false);
		button_Eliminar.setFont(new Font("Dialog", Font.PLAIN, 15));
		button_Eliminar.setBounds(268, 33, 103, 30);
		contentPane.add(button_Eliminar);

		this.setLocationRelativeTo(padre);
	}

	// INICIO VARIABLES/OBJETOS Y METODOS CREADOS POR EL DESARROLADOR
	protected void guardarGenero() {
		b_clases_VideoClub.Genero genero;

		switch (button_Guardar.getText()) {

		case "Guardar":
			String generoIntroducido = jTextField_Descripcion.getText();

			if (generoIntroducido.length() == 0) {// COMPROBAMOS SI LA DESCRIPCION NO ESTA VACIA
				A_Init_and_Config.mostrarMensaje_A_Usuario("�Debe introducir una Descripci�n!",
						"Error en Descripci�n");
				jTextField_Descripcion.grabFocus();
			} else {

				try {// PRIMERO COMPROBAMOS QUE EL GENERO INTRODUCIDO NO EXISTA YA ALMACENADO EN LA
						// TABLA GENEROS
					if (!c_Persistencia_MySQL.consultar_Existencia_Objeto("genero", jTextField_Descripcion.getText())) {
						// SINO EXISTE PROCEDEMOS A GUARDAR EL GENERO CON LA DESCRIPCION INTRODUCIDA
						genero = new Genero(generoIntroducido.trim());
						c_Persistencia_MySQL.guardarObjeto("genero", genero);
						A_Init_and_Config.mostrarMensaje_A_Usuario("Genero guardado correctamente", "Guardado");
						jTextField_Descripcion.setText("");
						jTextField_Descripcion.grabFocus();
						button_Guardar.setEnabled(false);

					} else {
						// CANCELAMOS LA OPERACION PORQUE LA DESCRIPCION INTRODUCIDA YA EXISTE
						A_Init_and_Config.mostrarMensaje_A_Usuario("La Descripcion introducida ya Existe", "Coincidencia Encontrada");
						jTextField_Descripcion.grabFocus();
						jTextField_Descripcion.setText("");
					}
				} catch (Exception e) {

					A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception");
				}
			}
			break;

		case "Modificar":
			String descripcionActual = jTextField_Descripcion.getText();

			if (descripcionActual.equals(descripcion_Original)) {
				A_Init_and_Config.mostrarMensaje_A_Usuario("No se ha modificado la Descripci�n",
						"Descripcion no Modificada");
				jTextField_Descripcion.grabFocus();
			} else {// MODIFICAMOS LA DESCRIPCION Y REANUDAMOS LA OPERACION

				try {

					genero = new Genero(Integer.valueOf(jTextfield_id_Genero.getText()),
							jTextField_Descripcion.getText());
					c_Persistencia_MySQL.modificarObjeto("genero", genero);
					A_Init_and_Config.mostrarMensaje_A_Usuario("Genero Modificado Correctamente", "Modificado");
					jTextfield_id_Genero.setText("");
					jTextField_Descripcion.setText("");
					jTextField_Descripcion.grabFocus();
					button_Guardar.setEnabled(false);
					button_Guardar.setText("Guardar");
					button_Eliminar.setVisible(false);

				} catch (Exception e) {
					A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception");
				}
			}

			break;
		}
	}

	protected void eliminarGenero() {

		if (A_Init_and_Config.mostrarMensaje_Si_No("�Esta seguro de Eliminar el G�nero?",
				"�Alerta!") == JOptionPane.YES_NO_OPTION) {
			// Ha seleccionado SI asi que borramos
			try {
				c_Persistencia_MySQL.borrarObjeto("genero", jTextfield_id_Genero.getText());
				A_Init_and_Config.mostrarMensaje_A_Usuario("G�nero Eliminado Correctamente", "Eliminado");
				cancelar();
			} catch (Exception e) {
				jTextField_Descripcion.grabFocus();
				e.printStackTrace();
			}
		} else {
			// Ha seleccionado NO asi que volvemos al estado en el que nos encontrabamos
			jTextField_Descripcion.grabFocus();
		}
	}

	protected void cancelar() {
		jTextfield_id_Genero.setText("");

		jTextField_Descripcion.setText("");
		jTextField_Descripcion.grabFocus();

		button_Guardar.setText("Guardar");
		button_Guardar.setEnabled(false);

		button_Eliminar.setVisible(false);
		button_Eliminar.setEnabled(false);
	}

	private void abrirVentanaBusquedaGenero() {

		ResultSet resultSet = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("genero",
				jTextField_Descripcion.getText());

		if (resultSet != null) {
			// Rutina de apertura modal de otra ventana
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			this.setEnabled(false);

			A1_Genero_Secundaria_GUI a1_Genero_Secundaria_GUI = new A1_Genero_Secundaria_GUI(this);
			a1_Genero_Secundaria_GUI.setVisible(true);

		}else {
			int resp = A_Init_and_Config.mostrarMensaje_Si_No("No hay generos que contengan " + jTextField_Descripcion.getText() +"\n �Quiere Guardarlo?", "G�nero no Encontrado");
			if (resp == JOptionPane.YES_OPTION) {
				Genero genero = new Genero(jTextField_Descripcion.getText());
				try {
					c_Persistencia_MySQL.guardarObjeto("genero", genero);
					A_Init_and_Config.mostrarMensaje_A_Usuario("Genero guardado correctamente", "Guardado");
					jTextField_Descripcion.setText("");
					jTextField_Descripcion.grabFocus();
					button_Guardar.setEnabled(false);
				} catch (Exception e) {
					A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception");
				}			
			} else {
				cancelar();
			}
		}

	}
}
