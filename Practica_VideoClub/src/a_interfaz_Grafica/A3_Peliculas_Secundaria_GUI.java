package a_interfaz_Grafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;

@SuppressWarnings("serial")
public class A3_Peliculas_Secundaria_GUI extends JFrame {

	private JPanel contentPane;
	public JTable jtablePeliculas;

	public Connection connection;

	static javax.swing.JFrame padre;

	public C_Persistencia_MySQL c_Persistencia_MySQL = A_Init_and_Config.c_Persistencia_MySQL;

	public JLabel jlabelTitulo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					A3_Peliculas_Secundaria_GUI frame = new A3_Peliculas_Secundaria_GUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// CONTROL DE LO QUE REALIZA LA APLICACION TRAS LA PULSACION DEL BOTON SALIR
	// SOBRE EL FRAME
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			// HABILITA EL FRAME DEL PADRE
			padre.setEnabled(true);
			padre.toFront();
			padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		}
	}

	// CONSTRUCTORES DE LA CLASE - Creamos Clase y Lanzamos FRAME

	// CONSTRUCTOR CON FRAME PADRE
	public A3_Peliculas_Secundaria_GUI(javax.swing.JFrame padre) {
		setResizable(false);

		// REALIZAMOS LAS ACCIONES PERTINENTES PARA ESTAR LIGADO AL FRAME PADRE
		A3_Peliculas_Secundaria_GUI.padre = padre;
		this.toFront();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setTitle("Listado de Peliculas");
		setBounds(100, 100, 469, 397);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("Button.select"));
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton buttonNuevaPelicula = new JButton("Nueva");
		buttonNuevaPelicula.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				volver();
				
				A3_Peliculas_Principal_GUI.button_Guardar.setEnabled(true);
				A3_Peliculas_Principal_GUI.button_Guardar.setText("Guardar");
				A3_Peliculas_Principal_GUI.button_Generos.setEnabled(false);
				A3_Peliculas_Principal_GUI.button_Actores.setEnabled(false);
				A3_Peliculas_Principal_GUI.button_Eliminar.setVisible(false);
				
				A3_Peliculas_Principal_GUI.jTextfield_id_Pelicula.setText(null);
				
				A3_Peliculas_Principal_GUI.jTextField_Titulo.setText(null);
				A3_Peliculas_Principal_GUI.jTextField_Titulo.grabFocus();
				
				A3_Peliculas_Principal_GUI.textArea_Sinopsis.setEnabled(true);
				A3_Peliculas_Principal_GUI.textArea_Sinopsis.setText(null);
				
				A3_Peliculas_Principal_GUI.jTextField_PrecioDia.setEnabled(true);
				A3_Peliculas_Principal_GUI.jTextField_PrecioDia.setText(null);
			}
		});
		buttonNuevaPelicula.setBounds(100, 317, 117, 25);
		contentPane.add(buttonNuevaPelicula);

		JButton button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				volver();
			}
		});
		button_Cancelar.setBounds(255, 317, 117, 25);
		contentPane.add(button_Cancelar);

		// Tras crear los controles de la Ventana Secundaria de Busqueda de Genero,
		// procedemos
		// A cargar en la JTABLE de esta nueva ventana toda la informacion referente a
		// los Generos
		// filtrada por la busqueda introducida, sino genera ningun resultado, se
		// muestran todos las
		// descripciones almacenadas

		jtablePeliculas = new JTable();
		jtablePeliculas.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		jtablePeliculas.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent mouseEvent) {
				if (mouseEvent.getClickCount() > 1) {
					int row = jtablePeliculas.getSelectedRow();

					String id_pelicula = jtablePeliculas.getValueAt(row, 0).toString();
					String titulo = jtablePeliculas.getValueAt(row, 1).toString();
					String sinopsis = jtablePeliculas.getValueAt(row, 2).toString();
					String precioDia = jtablePeliculas.getValueAt(row, 3).toString();
					
					// Devolvemos datos de la seleccion a la anterior Ventana
					A3_Peliculas_Principal_GUI.jTextfield_id_Pelicula.setText(id_pelicula);
					A3_Peliculas_Principal_GUI.jTextField_Titulo.setText(titulo);					
					A3_Peliculas_Principal_GUI.textArea_Sinopsis.setText(sinopsis);
					A3_Peliculas_Principal_GUI.textArea_Sinopsis.setEnabled(true);
					A3_Peliculas_Principal_GUI.jTextField_PrecioDia.setText(precioDia);
					A3_Peliculas_Principal_GUI.jTextField_PrecioDia.setEnabled(true);
					A3_Peliculas_Principal_GUI.button_Generos.setEnabled(true);
					A3_Peliculas_Principal_GUI.button_Actores.setEnabled(true);
					
					A3_Peliculas_Principal_GUI.titulo_Original = titulo;
					A3_Peliculas_Principal_GUI.sinopsis_Original = sinopsis;
					A3_Peliculas_Principal_GUI.precioDia_Original = precioDia;
					
					// Modifico el texto del boton Guardar para que modifique
					A3_Peliculas_Principal_GUI.button_Guardar.setEnabled(true);
					A3_Peliculas_Principal_GUI.button_Guardar.setText("Modificar");
					// Y habilitamos la opcion de Eliminar si lo que se desea es eliminar el Genero
					A3_Peliculas_Principal_GUI.button_Eliminar.setVisible(true);
					A3_Peliculas_Principal_GUI.button_Eliminar.setEnabled(true);

					volver();

				}
			}
		});
		
		//COMPONENTE JTABLE
		jtablePeliculas.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		jtablePeliculas.setModel(
				new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"id_pelicula", "Titulo", "Sinopsis", "PrecioDia"
			}
		));
		jtablePeliculas.setBounds(43, 96, 380, 198);
		contentPane.add(jtablePeliculas);			

		DefaultTableModel modelo = new DefaultTableModel();

		try {
			// modelo.addColumn("Id_Género");
			// modelo.addColumn("Descripción");
			modelo.setColumnIdentifiers(new String[] { "id_pelicula", "Titulo", "Sinopsis", "PrecioDia"});
			
			ResultSet resultSet = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("pelicula",
					A3_Peliculas_Principal_GUI.jTextField_Titulo.getText());

			Object[] fila = new Object[4]; // numeroColumnas es el número de columnas de la tabla y del ResultSet

			resultSet.beforeFirst();

			while (resultSet.next()) {

				for (int i = 0; i < 4; i++) {
					fila[i] = resultSet.getObject(i + 1);
				}
				modelo.addRow(fila);
			}

			jtablePeliculas.setModel(modelo);

			jlabelTitulo = new JLabel("Peliculas que contienen ");
			jlabelTitulo.setFont(new Font("Tahoma", Font.BOLD, 15));
			jlabelTitulo.setHorizontalAlignment(SwingConstants.CENTER);
			jlabelTitulo.setBounds(100, 23, 272, 32);
			contentPane.add(jlabelTitulo);

			JLabel jlabelSubTitutlo = new JLabel("Para modificar una Pelicula haga doble click sobre ella:");
			jlabelSubTitutlo.setHorizontalAlignment(SwingConstants.CENTER);
			jlabelSubTitutlo.setFont(new Font("Tahoma", Font.PLAIN, 10));
			jlabelSubTitutlo.setBounds(100, 60, 272, 25);
			contentPane.add(jlabelSubTitutlo);

		} catch (SQLException e) {
			A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "SQL Exception");
			e.printStackTrace();
		}

		this.setLocationRelativeTo(padre);
		
		String titulo = A3_Peliculas_Principal_GUI.jTextField_Titulo.getText();

		if (titulo.length() == 0) {
			jlabelTitulo.setText("Listado Completo de Peliculas");
		} else {
			 jlabelTitulo.setText("Peliculas que contienen : " +
					 A3_Peliculas_Principal_GUI.jTextField_Titulo.getText());
		}
	}

	public void volver() {

		this.dispose();
		padre.setEnabled(true);
		padre.toFront();
		padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);	
		A3_Peliculas_Principal_GUI.jTextField_Titulo.grabFocus();

	}
}
