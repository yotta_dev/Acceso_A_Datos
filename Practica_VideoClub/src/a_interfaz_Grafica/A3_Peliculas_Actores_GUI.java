package a_interfaz_Grafica;

import java.awt.Component;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;

import b_clases_VideoClub.Actor;
import b_clases_VideoClub.Genero;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.naming.spi.DirStateFactory.Result;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class A3_Peliculas_Actores_GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	static javax.swing.JFrame padre;

	public C_Persistencia_MySQL c_Persistencia_MySQL = A_Init_and_Config.c_Persistencia_MySQL;
	public static JList jlist_Actores_Actuales;
	public static JList jlist_Actores_Disponibles;
	public static Vector<String> actoresActualesAntesdeModificar = new Vector<String>();
	public static Vector<String> actoresDisponiblesAntesdeModificar = new Vector<String>();

	private Vector<String> actoresActuales;
	private Vector<String> actoresDisponibles;

	private JScrollPane scrollPane_Derecha;
	private JScrollPane scrollPane_Izquierda;
	private JButton button_Guardar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A3_Peliculas_Actores_GUI frame = new A3_Peliculas_Actores_GUI(padre);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// CONTROL DE LO QUE REALIZA LA APLICACION TRAS LA PULSACION DEL BOTON SALIR
	// SOBRE EL FRAME
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			// HABILITA EL FRAME DEL PADRE
			padre.setEnabled(true);
			padre.toFront();
			padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		}
	}

	/**
	 * Create the frame.
	 */
	public A3_Peliculas_Actores_GUI(javax.swing.JFrame padre) {
		setTitle("Actores que actuan en la Pelicula");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 481, 404);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblActoresactricesQueActuan = new JLabel("Actores/Actrices que actuan");
		lblActoresactricesQueActuan.setHorizontalAlignment(SwingConstants.CENTER);
		lblActoresactricesQueActuan.setBounds(10, 23, 216, 14);
		contentPane.add(lblActoresactricesQueActuan);

		scrollPane_Izquierda = new JScrollPane();
		scrollPane_Izquierda.setBounds(33, 43, 162, 253);
		contentPane.add(scrollPane_Izquierda);

		/* COMIENZO JLIST DE LA IZQUIERDA */
		String idpeliculaActual = A3_Peliculas_Principal_GUI.jTextfield_id_Pelicula.getText();

		ResultSet resultSet_Actores_Actuales = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("actua",
				idpeliculaActual);
		if (resultSet_Actores_Actuales != null) {

			try {
				resultSet_Actores_Actuales.beforeFirst();
				actoresActuales = new Vector<String>();
				while (resultSet_Actores_Actuales.next()) {
					actoresActuales.add(resultSet_Actores_Actuales.getString("nombre"));
				}

				jlist_Actores_Actuales = new JList(actoresActuales);
				jlist_Actores_Actuales.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
				actoresActualesAntesdeModificar = actoresActuales;
				scrollPane_Izquierda.setViewportView(jlist_Actores_Actuales);
			} catch (SQLException e) {
				A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception JList Actores Actuales");
			}
		} else {
			actoresActuales = new Vector<String>();
			jlist_Actores_Actuales = new JList();
			jlist_Actores_Actuales.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Izquierda.setViewportView(jlist_Actores_Actuales);
		}
		/* FIN JLIST DE LA IZQUIERDA */

		JLabel lblActoresDisponibles = new JLabel("Actores Disponibles");
		lblActoresDisponibles.setHorizontalAlignment(SwingConstants.CENTER);
		lblActoresDisponibles.setBounds(272, 23, 160, 14);
		contentPane.add(lblActoresDisponibles);

		scrollPane_Derecha = new JScrollPane();
		scrollPane_Derecha.setBounds(272, 40, 160, 255);
		contentPane.add(scrollPane_Derecha);

		/* COMIENZO JLIST DE LA DERECHA */
		ResultSet resultSet_Generos_Disponibles = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("actua", null);
		if (resultSet_Generos_Disponibles != null) {
			try {
				resultSet_Generos_Disponibles.beforeFirst();
				actoresDisponibles = new Vector<String>();
				while (resultSet_Generos_Disponibles.next()) {
					actoresDisponibles.add(resultSet_Generos_Disponibles.getString("nombre"));
				}

				if (actoresActuales != null) {
					for (int i = 0; i < actoresDisponibles.size(); i++) {
						for (int j = 0; j < actoresActuales.size(); j++) {
							if (actoresActuales.get(j).equals(actoresDisponibles.get(i))) {
								actoresDisponibles.remove(i);
							}
						}
					}
				}
				jlist_Actores_Disponibles = new JList(actoresDisponibles);
				jlist_Actores_Disponibles.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
				actoresDisponiblesAntesdeModificar = actoresDisponibles;
				scrollPane_Derecha.setViewportView(jlist_Actores_Disponibles);
			} catch (SQLException e) {
				A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception JList Generos Disponibles");
			}
		} else {
			jlist_Actores_Disponibles = new JList();
			jlist_Actores_Disponibles.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Derecha.setViewportView(jlist_Actores_Disponibles);
		}
		/* FIN JLIST DE LA DERECHA */

		JButton button_Aniadir = new JButton("<");
		button_Aniadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aniadirActor_Pelicula(jlist_Actores_Disponibles.getSelectedIndex());
			}
		});
		button_Aniadir.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_Aniadir.setBounds(216, 176, 40, 35);
		contentPane.add(button_Aniadir);

		JButton button_Quitar = new JButton(">");
		button_Quitar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quitarActor_Pelicula(jlist_Actores_Actuales.getSelectedIndex());
			}
		});
		button_Quitar.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_Quitar.setBounds(216, 130, 40, 35);
		contentPane.add(button_Quitar);

		button_Guardar = new JButton("Guardar");
		button_Guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				almacenarActoresPelicula();
			}
		});
		button_Guardar.setEnabled(false);
		button_Guardar.setBounds(121, 323, 89, 23);
		contentPane.add(button_Guardar);

		JButton button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelar();
			}
		});
		button_Cancelar.setBounds(257, 323, 89, 23);
		contentPane.add(button_Cancelar);

		A3_Peliculas_Actores_GUI.padre = padre;
		this.setLocationRelativeTo(padre);
		this.toFront();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	protected void almacenarActoresPelicula() {

		try {
			// Primero limpio los registros de genero que hubiese asociados a la pelicula
			c_Persistencia_MySQL.borrarObjeto("actua", A3_Peliculas_Principal_GUI.jTextfield_id_Pelicula.getText());

			// Y despues relleno de nuevo la tabla con los generos seleccionados por el
			// usuario
			for (int i = 0; i < actoresActuales.size(); i++) {
				Actor actorQueActuaEnPelicula = new Actor(// Objecto actor con campo idactor con valor idpelicula
						actoresActuales.get(i),
						Integer.valueOf(A3_Peliculas_Principal_GUI.jTextfield_id_Pelicula.getText()));
				// primero nombre y despues pelicula
				c_Persistencia_MySQL.guardarObjeto("actua", actorQueActuaEnPelicula);
			}

			A_Init_and_Config.mostrarMensaje_A_Usuario("Se han almacenado los cambios Correctamente",
					"Actores Modificados");
			cancelar();
		} catch (Exception e) {

			A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception al almacenar Actores");

		}

	}

	protected void aniadirActor_Pelicula(int selectedIndex) {
		if (selectedIndex != -1) {
			button_Guardar.setEnabled(true);
			actoresActuales.add(jlist_Actores_Disponibles.getSelectedValue().toString());

			jlist_Actores_Actuales = new JList(actoresActuales);
			jlist_Actores_Actuales.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Izquierda.setViewportView(jlist_Actores_Actuales);

			// Eliminamos de la Lista de Disponibles
			actoresDisponibles.remove(selectedIndex);
			jlist_Actores_Disponibles = new JList(actoresDisponibles);
			jlist_Actores_Disponibles.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Derecha.setViewportView(jlist_Actores_Disponibles);
			// A�adimos a la Lista de Generos de la Pelicula

		}

	}

	protected void quitarActor_Pelicula(int selectedIndex) {
		if (selectedIndex != -1) {
			button_Guardar.setEnabled(true);

			actoresDisponibles.add(jlist_Actores_Actuales.getSelectedValue().toString());

			jlist_Actores_Disponibles = new JList(actoresDisponibles);
			jlist_Actores_Disponibles.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Derecha.setViewportView(jlist_Actores_Disponibles);

			// Eliminamos de la lista de Generos Actuales
			actoresActuales.remove(selectedIndex);
			jlist_Actores_Actuales = new JList(actoresActuales);
			jlist_Actores_Actuales.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Izquierda.setViewportView(jlist_Actores_Actuales);
		}

	}

	protected void cancelar() {
		this.dispose();
		padre.setEnabled(true);
		padre.toFront();
		padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		A3_Peliculas_Principal_GUI.jTextField_Titulo.grabFocus();
	}
}
