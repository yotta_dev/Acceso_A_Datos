package a_interfaz_Grafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import com.mysql.jdbc.ResultSet;

import b_clases_VideoClub.Actor;

@SuppressWarnings("serial")
public class A2_Actores_Principal_GUI extends JFrame {

	private JPanel contentPane;
	private JLabel label_Nombre;

	// JTEXFIELD PUBLICOS PARA PODER HACER SETTEXT CUANDO LA VENTANA DE
	// BUSQUEDA DEVUELVA UN RESULTADO
	public static JTextField jTextfield_id_Actor;
	public static JTextField jTextField_Nombre;
	public static JTextField jTextField_FechaNac;

	public C_Persistencia_MySQL c_Persistencia_MySQL = A_Init_and_Config.c_Persistencia_MySQL;

	// Objecto Frame Padre para realizar operaciones Modales
	static javax.swing.JFrame padre;

	// Boton Guardar para ser activado o no si procede
	public static JButton button_Guardar;
	public static JButton button_Eliminar;

	public static String nombre_Original;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// Visualizacion del Frame
					A2_Actores_Principal_GUI frame = new A2_Actores_Principal_GUI(padre);
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// CONTROL DE LO QUE REALIZA EL FRAME TRAS LA PULSACION DEL BOTON SALIR
	// SOBRE EL FRAME
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			// HABILITA EL FRAME DEL PADRE
			padre.setEnabled(true);
			padre.toFront();
			padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		}
	}

	public A2_Actores_Principal_GUI(javax.swing.JFrame padre) {
		setResizable(false);
		// REALIZAMOS LAS ACCIONES PERTINENTES PARA ESTAR LIGADO AL FRAME PADRE
		A2_Actores_Principal_GUI.padre = padre;
		this.toFront();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		setBackground(UIManager.getColor("Button.background"));
		setTitle("Actores");
		setBounds(100, 100, 428, 257);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("ComboBox.selectionBackground"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label_Actor = new JLabel("id_Actor :");
		label_Actor.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_Actor.setBounds(55, 36, 94, 23);
		contentPane.add(label_Actor);

		jTextfield_id_Actor = new JTextField();
		jTextfield_id_Actor.setFont(new Font("Tahoma", Font.PLAIN, 15));
		jTextfield_id_Actor.setEnabled(false);
		jTextfield_id_Actor.setBounds(162, 33, 94, 30);
		contentPane.add(jTextfield_id_Actor);
		jTextfield_id_Actor.setColumns(10);

		label_Nombre = new JLabel("Nombre :");
		label_Nombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_Nombre.setBounds(54, 83, 95, 15);
		contentPane.add(label_Nombre);

		jTextField_Nombre = new JTextField();
		jTextField_Nombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		jTextField_Nombre.setBounds(163, 76, 165, 30);
		contentPane.add(jTextField_Nombre);
		jTextField_Nombre.setColumns(10);

		JLabel lblFechaNac = new JLabel("Fecha Nac :");
		lblFechaNac.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblFechaNac.setBounds(55, 124, 95, 15);
		contentPane.add(lblFechaNac);

		button_Guardar = new JButton("Guardar");
		button_Guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				guardarActor();
			}
		});

		jTextField_FechaNac = new JTextField();
		jTextField_FechaNac.setEnabled(false);
		jTextField_FechaNac.setFont(new Font("Tahoma", Font.PLAIN, 15));
		jTextField_FechaNac.setColumns(10);
		jTextField_FechaNac.setBounds(164, 117, 165, 30);
		contentPane.add(jTextField_FechaNac);
		button_Guardar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		button_Guardar.setEnabled(false);
		button_Guardar.setBounds(83, 172, 111, 30);
		contentPane.add(button_Guardar);

		JButton buttonCancelar = new JButton("Cancelar");
		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cancelar();

			}
		});
		buttonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		buttonCancelar.setBounds(232, 172, 111, 30);
		contentPane.add(buttonCancelar);

		JButton button_Buscar = new JButton("");
		button_Buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Posteriormente creo que aqui tengo que implementar que la cadena Descripcion
				// este validada de cierta manera
				abrirVentanaBusquedaActores();

			}
		});
		button_Buscar.setBorderPainted(false);
		button_Buscar.setBorder(null);
		button_Buscar.setIcon(new ImageIcon(
				"C:\\Users\\TheSlayeOne1\\Desktop\\Repositorios\\Acceso_A_Datos\\Practica_VideoClub\\resources\\imgs\\loupe_78347.png"));
		button_Buscar.setBounds(338, 76, 33, 30);
		contentPane.add(button_Buscar);

		button_Eliminar = new JButton("Eliminar");
		button_Eliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				eliminarGenero();
			}
		});
		button_Eliminar.setVisible(false);
		button_Eliminar.setFont(new Font("Dialog", Font.PLAIN, 15));
		button_Eliminar.setBounds(268, 33, 103, 30);
		contentPane.add(button_Eliminar);

		this.setLocationRelativeTo(padre);
	}

	// INICIO VARIABLES/OBJETOS Y METODOS CREADOS POR EL DESARROLADOR
	protected void guardarActor() {
		b_clases_VideoClub.Actor actor;
		String idActor = jTextfield_id_Actor.getText();
		String nombreIntroducido = jTextField_Nombre.getText();
		String fechaIntroducida = jTextField_FechaNac.getText();
		
		switch (button_Guardar.getText()) {
		case "Guardar":
			if (nombreIntroducido.length() == 0||fechaIntroducida.length()==0) {// COMPROBAMOS SI LA DESCRIPCION NO ESTA VACIA
				A_Init_and_Config.mostrarMensaje_A_Usuario("�Debe introducir un Nombre y una Fecha!", "Error en Campos");
				jTextField_Nombre.grabFocus();
			} else {

				try {// PRIMERO COMPROBAMOS QUE EL GENERO INTRODUCIDO NO EXISTA YA ALMACENADO EN LA
						// TABLA GENEROS
					if (!c_Persistencia_MySQL.consultar_Existencia_Objeto("actor", jTextField_Nombre.getText())) {
						// SINO EXISTE PROCEDEMOS A GUARDAR EL GENERO CON LA DESCRIPCION INTRODUCIDA
						Date date_FechaIntroducida = Date.valueOf(jTextField_FechaNac.getText());//FUTURA VALIDACION
						actor = new Actor(nombreIntroducido.trim(), date_FechaIntroducida);
						c_Persistencia_MySQL.guardarObjeto("actor", actor);
						A_Init_and_Config.mostrarMensaje_A_Usuario("Actor guardado correctamente", "Guardado");
						jTextField_Nombre.setText("");
						jTextField_Nombre.grabFocus();
						jTextField_FechaNac.setEnabled(false);
						jTextField_FechaNac.setText("");
						button_Guardar.setEnabled(false);

					} else {
						// CANCELAMOS LA OPERACION PORQUE LA DESCRIPCION INTRODUCIDA YA EXISTE
						A_Init_and_Config.mostrarMensaje_A_Usuario("El Nombre introducido ya Existe",
								"Coincidencia Encontrada");
						jTextField_Nombre.grabFocus();
						jTextField_Nombre.setText("");
					}
				} catch (Exception e) {

					A_Init_and_Config.mostrarMensaje_A_Usuario("El formato de la Fecha es YYYY-MM-DD", "Error en Fecha");
					A2_Actores_Principal_GUI.jTextField_FechaNac.grabFocus();
				}
			}
			break;

		case "Modificar":
			String nombreActual = jTextField_Nombre.getText();

			if (nombreActual.equals(nombre_Original)) {
				A_Init_and_Config.mostrarMensaje_A_Usuario("No se ha modificado el Nombre", "Nombre no Modificado");
				jTextField_Nombre.grabFocus();
			} else {// MODIFICAMOS LA DESCRIPCION Y REANUDAMOS LA OPERACION

				try {
					Date date_FechaIntroducida = Date.valueOf(jTextField_FechaNac.getText());
					actor = new Actor(Integer.valueOf(idActor),nombreIntroducido.trim(), date_FechaIntroducida);
					c_Persistencia_MySQL.modificarObjeto("actor", actor);
					A_Init_and_Config.mostrarMensaje_A_Usuario("Actor Modificado Correctamente", "Modificado");
					jTextfield_id_Actor.setText("");
					jTextField_Nombre.setText("");
					jTextField_Nombre.grabFocus();
					jTextField_FechaNac.setEnabled(false);
					jTextField_FechaNac.setText("");
					button_Guardar.setEnabled(false);
					button_Guardar.setText("Guardar");
					button_Eliminar.setVisible(false);

				} catch (Exception e) {
					A_Init_and_Config.mostrarMensaje_A_Usuario("El formato de la Fecha es YYYY-MM-DD", "Error en Fecha");
					A2_Actores_Principal_GUI.jTextField_FechaNac.grabFocus();
				}
			}

			break;
		}
	}

	protected void eliminarGenero() {

		if (A_Init_and_Config.mostrarMensaje_Si_No("�Esta seguro de Eliminar el Actor/Actriz?",
				"�Alerta!") == JOptionPane.YES_NO_OPTION) {
			// Ha seleccionado SI asi que borramos
			try {
				c_Persistencia_MySQL.borrarObjeto("actor", jTextfield_id_Actor.getText());
				A_Init_and_Config.mostrarMensaje_A_Usuario("Actor Eliminado Correctamente", "Eliminado");
				cancelar();
			} catch (Exception e) {
				jTextField_Nombre.grabFocus();
				e.printStackTrace();
			}
		} else {
			// Ha seleccionado NO asi que volvemos al estado en el que nos encontrabamos
			jTextField_Nombre.grabFocus();
		}
	}

	protected void cancelar() {
		jTextfield_id_Actor.setText("");

		jTextField_Nombre.setText("");
		jTextField_Nombre.grabFocus();
		
		jTextField_FechaNac.setEnabled(false);
		jTextField_FechaNac.setText("");

		button_Guardar.setText("Guardar");
		button_Guardar.setEnabled(false);

		button_Eliminar.setVisible(false);
		button_Eliminar.setEnabled(false);
	}

	private void abrirVentanaBusquedaActores() {

		ResultSet resultSet = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("actor", jTextField_Nombre.getText());

		if (resultSet != null) {
			// Rutina de apertura modal de otra ventana
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			this.setEnabled(false);

			A2_Actores_Secundaria_GUI a2_Actores_Secundaria_GUI = new A2_Actores_Secundaria_GUI(this);
			a2_Actores_Secundaria_GUI.setVisible(true);

		} else {
			int resp = A_Init_and_Config.mostrarMensaje_Si_No(
					"No hay Actores con el Nombre " + jTextField_Nombre.getText() + "\n �Quiere Guardarlo?",
					"Nombre no Encontrado");
			if (resp == JOptionPane.YES_OPTION) {

				try {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
					Actor actor = new Actor(jTextField_Nombre.getText().trim(), simpleDateFormat.parse(jTextField_FechaNac.getText()));
					c_Persistencia_MySQL.guardarObjeto("actor", actor);
					A_Init_and_Config.mostrarMensaje_A_Usuario("Actor guardado correctamente", "Guardado");
					jTextField_Nombre.setText("");
					jTextField_Nombre.grabFocus();
					button_Guardar.setEnabled(false);
				} catch (Exception e) {
					A_Init_and_Config.mostrarMensaje_A_Usuario("El formato de la Fecha es YYYY-MM-DD", "Error en Fecha");
					A2_Actores_Principal_GUI.jTextField_FechaNac.grabFocus();
				}
			} else {
				cancelar();
			}
		}

	}
}
