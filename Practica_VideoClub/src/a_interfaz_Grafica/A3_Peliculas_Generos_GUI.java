package a_interfaz_Grafica;

import java.awt.Component;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;

import b_clases_VideoClub.Genero;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.naming.spi.DirStateFactory.Result;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class A3_Peliculas_Generos_GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	public Connection connection;

	static javax.swing.JFrame padre;

	public C_Persistencia_MySQL c_Persistencia_MySQL = A_Init_and_Config.c_Persistencia_MySQL;
	public static JList jlist_Generos_Actuales;
	public static JList jlist_Generos_Disponibles;
	public static Vector<String> generosActualesAntesdeModificar = new Vector<String>();
	public static Vector<String> generosDisponiblesAntesdeModificar = new Vector<String>();

	private Vector<String> generosActuales;
	private Vector<String> generosDisponibles;

	private JScrollPane scrollPane_Derecha;
	private JScrollPane scrollPane_Izquierda;
	private JButton button_Guardar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A3_Peliculas_Generos_GUI frame = new A3_Peliculas_Generos_GUI(padre);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// CONTROL DE LO QUE REALIZA LA APLICACION TRAS LA PULSACION DEL BOTON SALIR
	// SOBRE EL FRAME
	protected void processWindowEvent(java.awt.event.WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == java.awt.event.WindowEvent.WINDOW_CLOSING) {
			// HABILITA EL FRAME DEL PADRE
			padre.setEnabled(true);
			padre.toFront();
			padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		}
	}

	/**
	 * Create the frame.
	 */
	public A3_Peliculas_Generos_GUI(javax.swing.JFrame padre) {
		setTitle("Generos a los que pertenece la Pelicula");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 481, 404);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label_GenerosActuales = new JLabel("Generos a los que pertenece");
		label_GenerosActuales.setHorizontalAlignment(SwingConstants.CENTER);
		label_GenerosActuales.setBounds(10, 23, 216, 14);
		contentPane.add(label_GenerosActuales);

		scrollPane_Izquierda = new JScrollPane();
		scrollPane_Izquierda.setBounds(33, 43, 162, 253);
		contentPane.add(scrollPane_Izquierda);

		/* COMIENZO JLIST DE LA IZQUIERDA */
		String idpeliculaActual = A3_Peliculas_Principal_GUI.jTextfield_id_Pelicula.getText();

		ResultSet resultSet_Generos_Actuales = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("pertenece",
				idpeliculaActual);
		if (resultSet_Generos_Actuales != null) {

			try {
				resultSet_Generos_Actuales.beforeFirst();
				generosActuales = new Vector<String>();
				while (resultSet_Generos_Actuales.next()) {
					generosActuales.add(resultSet_Generos_Actuales.getString("descripcion"));
				}

				jlist_Generos_Actuales = new JList(generosActuales);
				jlist_Generos_Actuales.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
				generosActualesAntesdeModificar = generosActuales;
				scrollPane_Izquierda.setViewportView(jlist_Generos_Actuales);
			} catch (SQLException e) {
				A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception JList Generos Actuales");
			}
		} else {
			generosActuales = new Vector<String>();
			jlist_Generos_Actuales = new JList();
			jlist_Generos_Actuales.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Izquierda.setViewportView(jlist_Generos_Actuales);
		}
		/* FIN JLIST DE LA IZQUIERDA */

		JLabel label_Generos_Disponibles = new JLabel("Generos Disponibles");
		label_Generos_Disponibles.setHorizontalAlignment(SwingConstants.CENTER);
		label_Generos_Disponibles.setBounds(272, 23, 160, 14);
		contentPane.add(label_Generos_Disponibles);

		scrollPane_Derecha = new JScrollPane();
		scrollPane_Derecha.setBounds(272, 40, 160, 255);
		contentPane.add(scrollPane_Derecha);

		/* COMIENZO JLIST DE LA DERECHA */
		ResultSet resultSet_Generos_Disponibles = c_Persistencia_MySQL.obtenerResultSet_De_Tabla("pertenece", null);
		if (resultSet_Generos_Disponibles != null) {
			try {
				resultSet_Generos_Disponibles.beforeFirst();
				generosDisponibles = new Vector<String>();
				while (resultSet_Generos_Disponibles.next()) {
					generosDisponibles.add(resultSet_Generos_Disponibles.getString("descripcion"));
				}

				if (generosActuales != null) {
					for (int i = 0; i < generosDisponibles.size(); i++) {
						for (int j = 0; j < generosActuales.size(); j++) {
							if (generosActuales.get(j).equals(generosDisponibles.get(i))) {
								generosDisponibles.remove(i);
							}
						}
					}
				}
				jlist_Generos_Disponibles = new JList(generosDisponibles);
				jlist_Generos_Disponibles.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
				generosDisponiblesAntesdeModificar = generosDisponibles;
				scrollPane_Derecha.setViewportView(jlist_Generos_Disponibles);
			} catch (SQLException e) {
				A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception JList Generos Disponibles");
			}
		} else {
			jlist_Generos_Disponibles = new JList();
			jlist_Generos_Disponibles.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Derecha.setViewportView(jlist_Generos_Disponibles);
		}
		/* FIN JLIST DE LA DERECHA */

		JButton button_Aniadir = new JButton("<");
		button_Aniadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aniadirGenero_Pelicula(jlist_Generos_Disponibles.getSelectedIndex());
			}
		});
		button_Aniadir.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_Aniadir.setBounds(216, 176, 40, 35);
		contentPane.add(button_Aniadir);

		JButton button_Quitar = new JButton(">");
		button_Quitar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quitarGenero_Pelicula(jlist_Generos_Actuales.getSelectedIndex());
			}
		});
		button_Quitar.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_Quitar.setBounds(216, 130, 40, 35);
		contentPane.add(button_Quitar);

		button_Guardar = new JButton("Guardar");
		button_Guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				almacenarGenerosPelicula();
			}
		});
		button_Guardar.setEnabled(false);
		button_Guardar.setBounds(121, 323, 89, 23);
		contentPane.add(button_Guardar);

		JButton button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelar();
			}
		});
		button_Cancelar.setBounds(257, 323, 89, 23);
		contentPane.add(button_Cancelar);

		A3_Peliculas_Generos_GUI.padre = padre;
		this.setLocationRelativeTo(padre);
		this.toFront();
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	protected void almacenarGenerosPelicula() {

		try {
			// Primero limpio los registros de genero que hubiese asociados a la pelicula
			c_Persistencia_MySQL.borrarObjeto("pertenece", A3_Peliculas_Principal_GUI.jTextfield_id_Pelicula.getText());

			// Y despues relleno de nuevo la tabla con los generos seleccionados por el
			// usuario
			for (int i = 0; i < generosActuales.size(); i++) {
				Genero generoPerteneceAPelicula = new Genero(
						Integer.valueOf(A3_Peliculas_Principal_GUI.jTextfield_id_Pelicula.getText()),
						generosActuales.get(i));
				c_Persistencia_MySQL.guardarObjeto("pertenece", generoPerteneceAPelicula);
			}

			A_Init_and_Config.mostrarMensaje_A_Usuario("Se han almacenado los cambios Correctamente",
					"Generos Modificados");
			cancelar();
		} catch (Exception e) {

			A_Init_and_Config.mostrarMensaje_A_Usuario(e.getMessage(), "Exception al almacenar Generos");

		}

	}

	protected void aniadirGenero_Pelicula(int selectedIndex) {
		if (selectedIndex != -1) {
			button_Guardar.setEnabled(true);
			generosActuales.add(jlist_Generos_Disponibles.getSelectedValue().toString());
			
			jlist_Generos_Actuales = new JList(generosActuales);
			jlist_Generos_Actuales.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Izquierda.setViewportView(jlist_Generos_Actuales);

			// Eliminamos de la Lista de Disponibles
			generosDisponibles.remove(selectedIndex);
			jlist_Generos_Disponibles = new JList(generosDisponibles);
			jlist_Generos_Disponibles.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Derecha.setViewportView(jlist_Generos_Disponibles);
			// A�adimos a la Lista de Generos de la Pelicula

		}

	}

	protected void quitarGenero_Pelicula(int selectedIndex) {
		if (selectedIndex != -1) {
			button_Guardar.setEnabled(true);

			generosDisponibles.add(jlist_Generos_Actuales.getSelectedValue().toString());

			jlist_Generos_Disponibles = new JList(generosDisponibles);
			jlist_Generos_Disponibles.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Derecha.setViewportView(jlist_Generos_Disponibles);

			// Eliminamos de la lista de Generos Actuales
			generosActuales.remove(selectedIndex);
			jlist_Generos_Actuales = new JList(generosActuales);
			jlist_Generos_Actuales.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			scrollPane_Izquierda.setViewportView(jlist_Generos_Actuales);
		}

	}

	protected void cancelar() {
		this.dispose();
		padre.setEnabled(true);
		padre.toFront();
		padre.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		A3_Peliculas_Principal_GUI.jTextField_Titulo.grabFocus();
	}
}
