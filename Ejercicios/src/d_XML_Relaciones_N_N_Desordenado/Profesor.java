package d_XML_Relaciones_N_N_Desordenado;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Profesor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//ATRIBUTOS
	private int id;
	private String nombre;
	private String ape1;
	private String ape2;
	private Set<Modulo> modulos = new HashSet<Modulo>();
	
	//CONSTRUCTOR VACIO
	public Profesor() {
	}
	
	//CONSTRUCTOR CON DATOS
	public Profesor(int id, String nombre, String ape1, String ape2) {
		this.id = id;
		this.nombre = nombre;
		this.ape1 = ape1;
		this.ape2 = ape2;
	}
	
	//GETTERS AND SETTERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApe1() {
		return ape1;
	}

	public void setApe1(String ape1) {
		this.ape1 = ape1;
	}

	public String getApe2() {
		return ape2;
	}

	public void setApe2(String ape2) {
		this.ape2 = ape2;
	}

	public Set<Modulo> getModulos() {
		return modulos;
	}

	public void setModulos(Set<Modulo> modulos) {
		this.modulos = modulos;
	}				

}


