package jdbc_1;

public class Persona {
	
	String nombre;
	int codigo_postal;
	String pais;
	String email;
	
	public Persona(String nombre, int codigo_postal, String pais, String email) {
		super();
		this.nombre = nombre;
		this.codigo_postal = codigo_postal;
		this.pais = pais;
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(int codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}


