package jdbc_1;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;

public class PersistenciaBD implements Persistencia {

	String tipoBD;
	Connection con;

	/*
	 * persona               //tabla       0
	 * ejercicios            //bd          1
	 * com.mysql.jdbc.Driver //TipoBD      2
	 * localhost:3306        //IP          3
	 * root                  //Usuario     4
	 * manager               //Passwrd     5
	 */

	// Tipo BD varia dependiendo de la Base de Datos
	public PersistenciaBD(String bd,String tipoBD,String IP, String usu, String pass) throws Exception {
		try {

			Class.forName(tipoBD);

			if (tipoBD.equals("com.mysql.jdbc.Driver")) {
			  //con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ejercicios", "root", "manager");
			  
				con = (Connection) DriverManager.getConnection("jdbc:mysql://"+IP+"/"+bd,usu,pass);
			   
				System.out.println("Conectado");
			}
			// if para cada uno de los tipos de BBDD que vayamos a añadir

		} catch (ClassNotFoundException ex) {

			System.out.println("No se encontro el Driver MySQL para JDBC.");
			ex.printStackTrace();

		} catch (SQLException e) {

			System.out.println("Error en la conexión con la Base de Datos.");
			e.printStackTrace();
		}
	}

	@Override
	public void conectarDB(String IP, String usu, String pass, String bd) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void desconectarDB() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<Persona> listadoPersonas(String tabla, String orderBy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarPersona(String tabla, Persona p) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPersona(String tabla, String email) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Persona consultarPersona(String tabla, String email) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
