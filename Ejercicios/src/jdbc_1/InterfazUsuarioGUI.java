package jdbc_1;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import javax.swing.JButton;
import java.awt.Font;


public class InterfazUsuarioGUI extends JFrame {

	private JPanel contentPane;
	private JTable table;
	//Linea 70 > Hago GetModel de la tabla para poder manejarlo en un ambito Global
	private DefaultTableModel modelo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					InterfazUsuarioGUI frame = new InterfazUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					operacionesBBDD();
				} catch (FileNotFoundException e) {		
					JOptionPane.showMessageDialog(null, "No se ha encontrado el fichero de Configuración (CFG.INI)", "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Error en la Lectura del Fichero de Configuración (CFG.INI)", "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);

				}
				
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioGUI() {
		setTitle("Visualización Tablas BD");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 421);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable();
		table.setBorder(new LineBorder(new Color(0, 0, 0)));
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"Nombre", "Codigo Postal", "Pa\u00EDs", "Email"
			}
		));
		table.getColumnModel().getColumn(2).setPreferredWidth(90);
		table.getColumnModel().getColumn(3).setPreferredWidth(250);
		modelo = (DefaultTableModel)table.getModel();
		
		JLabel jLabelNombre = new JLabel("Nombre");
		jLabelNombre.setFont(new Font("Dialog", Font.BOLD, 11));
		jLabelNombre.setBorder(new MatteBorder(1, 1, 0, 0, (Color) new Color(0, 0, 0)));
		jLabelNombre.setHorizontalAlignment(SwingConstants.CENTER);
		jLabelNombre.setBounds(24, 33, 97, 22);
		contentPane.add(jLabelNombre);
		
		JLabel jLabelCodigoPostal = new JLabel("Codigo Postal");
		jLabelCodigoPostal.setFont(new Font("Dialog", Font.BOLD, 11));
		jLabelCodigoPostal.setHorizontalAlignment(SwingConstants.CENTER);
		jLabelCodigoPostal.setBorder(new MatteBorder(1, 1, 0, 0, (Color) new Color(0, 0, 0)));
		jLabelCodigoPostal.setBounds(120, 33, 97, 22);
		contentPane.add(jLabelCodigoPostal);
		
		JLabel jLabelPais = new JLabel("Pa\u00EDs");
		jLabelPais.setFont(new Font("Dialog", Font.BOLD, 11));
		jLabelPais.setHorizontalAlignment(SwingConstants.CENTER);
		jLabelPais.setBorder(new MatteBorder(1, 1, 0, 0, (Color) new Color(0, 0, 0)));
		jLabelPais.setBounds(217, 33, 112, 22);
		contentPane.add(jLabelPais);
		
		JLabel jLabelEmail = new JLabel("Email");
		jLabelEmail.setFont(new Font("Dialog", Font.BOLD, 11));
		jLabelEmail.setHorizontalAlignment(SwingConstants.CENTER);
		jLabelEmail.setBorder(new MatteBorder(1, 1, 0, 1, (Color) new Color(0, 0, 0)));
		jLabelEmail.setBounds(329, 33, 272, 22);
		contentPane.add(jLabelEmail);
		table.setBounds(24, 55, 577, 256);
		contentPane.add(table);
		
		JButton buttonListadoCompleto = new JButton("Listado Completo");
		buttonListadoCompleto.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonListadoCompleto.setBounds(626, 94, 221, 22);
		contentPane.add(buttonListadoCompleto);
		
		JButton buttonListadoCP = new JButton("Listado ordenado por CP");
		buttonListadoCP.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonListadoCP.setBounds(626, 128, 221, 22);
		contentPane.add(buttonListadoCP);
		
		JButton buttonListadoEmail = new JButton("Listado ordenado por Email");
		buttonListadoEmail.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonListadoEmail.setBounds(626, 162, 221, 22);
		contentPane.add(buttonListadoEmail);
		
		JButton buttonListadoNombre = new JButton("Listado ordenado por Nombre");
		buttonListadoNombre.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonListadoNombre.setBounds(626, 196, 221, 22);
		contentPane.add(buttonListadoNombre);
		
		JButton buttonListadoPais = new JButton("Listado ordenado por Pais");
		buttonListadoPais.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonListadoPais.setBounds(626, 230, 221, 22);
		contentPane.add(buttonListadoPais);
		
		JButton buttonAlta = new JButton("Dar de Alta Persona");
		buttonAlta.setBounds(24, 336, 221, 22);
		contentPane.add(buttonAlta);
		
		JButton buttonModificar = new JButton("Modificar Persona");
		buttonModificar.setBounds(316, 336, 221, 22);
		contentPane.add(buttonModificar);
		
		JButton buttonBorrar = new JButton("Borrar Persona");
		buttonBorrar.setBounds(604, 336, 221, 22);
		contentPane.add(buttonBorrar);
	}
	
	private static void operacionesBBDD() throws IOException {
		
		File ficheroConfiguracion = new File("CFG.INI");
		
		if (ficheroConfiguracion.exists()) {
			
			BufferedReader bufferedReader = new BufferedReader(new FileReader(ficheroConfiguracion));
			
			ArrayList<String>parametrosConexion = new ArrayList<>();
			
			String linea;
			
			while ((linea = bufferedReader.readLine()) != null) {
				
				parametrosConexion.add(linea);
				
			}
			
			try {
				//Creando la conexion a la BBDD
				PersistenciaBD bd = new PersistenciaBD(
						//NombreBD
						parametrosConexion.get(1),
						//DriverJDBC
						parametrosConexion.get(2),
						//IP
						parametrosConexion.get(3),
						//Usuario
						parametrosConexion.get(4),
						//Password
						parametrosConexion.get(5));
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
		
	}
}
