package editorDeTexto;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Color;

public class Editor extends JFrame {

	private JPanel contentPane;
	private JTextArea editorTextArea;
	File fichero;
	private JButton buttonGuardar;
	private JButton buttonAbrir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Editor frame = new Editor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Editor() {
		setBackground(SystemColor.activeCaptionText);
		setResizable(false);
		setTitle("Editor de Texto");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 706, 657);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.controlShadow);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(38, 37, 624, 464);
		contentPane.add(scrollPane);

		editorTextArea = new JTextArea();
		editorTextArea.setLineWrap(true);
		editorTextArea.setBackground(SystemColor.window);
		scrollPane.setViewportView(editorTextArea);

		buttonAbrir = new JButton("Abrir");
		buttonAbrir.setBackground(Color.LIGHT_GRAY);
		buttonAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				mostrarTextoEnJTextArea(seleccionarFichero());
			}
		});
		buttonAbrir.setBounds(185, 537, 106, 45);
		contentPane.add(buttonAbrir);

		buttonGuardar = new JButton("Guardar");
		buttonGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				guardarFichero();
			}
		});
		buttonGuardar.setEnabled(false);
		buttonGuardar.setBackground(Color.LIGHT_GRAY);
		buttonGuardar.setBounds(393, 537, 106, 45);
		contentPane.add(buttonGuardar);
	}

	/**
	 * Este metodo realiza multiples funciones que se activan tras seleccionar el
	 * JButton Abrir la primera accion que realiza es abrir un JFileChooser que
	 * permite seleccionar el fichero de texto que queremos editar utilizando
	 * previamente un filtro sobre ese JFIleChooser que fltra para que unicamente se
	 * puedan seleccionar ficheros del tipo .txt
	 * 
	 * @return
	 */
	private String seleccionarFichero() {
		String linea = "";
		String texto = "";

		try {
			JFileChooser selectorDeFicheros = new JFileChooser();

			FileNameExtensionFilter filtroParaElSelector = new FileNameExtensionFilter(".txt", "txt", "text");

			selectorDeFicheros.setFileFilter(filtroParaElSelector);

			int op = selectorDeFicheros.showOpenDialog(this);
			/*if (op == JFileChooser.APPROVE_OPTION) {

			}*/
			fichero = selectorDeFicheros.getSelectedFile();

			this.setTitle("Editor de Texto: " + fichero.getAbsolutePath());

			if (fichero != null) {
				FileReader lectorDeFicheros = new FileReader(fichero);
				BufferedReader bufferDelLector = new BufferedReader(lectorDeFicheros);

				while ((linea = bufferDelLector.readLine()) != null) {
					texto += linea + "\n";
				}

				bufferDelLector.close();
			}

			buttonGuardar.setEnabled(true);
			buttonAbrir.setEnabled(false);

		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, ex + "" + "\nNo se ha encontrado el archivo", "AVISO!",
					JOptionPane.WARNING_MESSAGE);
		} catch (NullPointerException ficheroNoSeleccionado) {
			JOptionPane.showMessageDialog(null, ficheroNoSeleccionado + "" + "\nNo se ha seleccionado un fichero",
					"AVISO!", JOptionPane.WARNING_MESSAGE);
		}
		return texto;
	}

	private void mostrarTextoEnJTextArea(String texto) {
		editorTextArea.setText(texto);

	}

	private void guardarFichero() {
		FileWriter ficheroAImprimir = null;
		PrintWriter imprimirContenidoFichero = null;

		try {

			ficheroAImprimir = new FileWriter(fichero.getAbsolutePath());

			imprimirContenidoFichero = new PrintWriter(ficheroAImprimir, false);

			imprimirContenidoFichero.print(editorTextArea.getText());

			buttonGuardar.setEnabled(false);
			buttonAbrir.setEnabled(true);

			JOptionPane.showMessageDialog(null, "\nEl fichero se ha guardado Correctamente", "AVISO!",
					JOptionPane.WARNING_MESSAGE);

			editorTextArea.setText("");

			this.setTitle("Editor de Texto");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != fichero)
					ficheroAImprimir.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}

	}

}
