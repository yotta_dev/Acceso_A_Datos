package dom_IndexarTexto;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class VerLineaLibro extends JFrame {

	private JPanel contentPane;
	private JTextField lineaLibrotextField;
	private JTextField tituloLibrotextField;
	private JScrollPane scrollPane;
	private JTextArea resultadoLineatextArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerLineaLibro frame = new VerLineaLibro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VerLineaLibro() {
		setTitle("Ver Linea Libro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 361, 181);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton buscarButton = new JButton("Buscar");
		buscarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					buscarLineaLibro(tituloLibrotextField, lineaLibrotextField, resultadoLineatextArea);
				} catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException e1) {
					JOptionPane.showInputDialog(this, "Se han cometido Errores" + e1.getMessage());
				}
			}
		});
		buscarButton.setBounds(227, 22, 95, 25);
		contentPane.add(buscarButton);

		tituloLibrotextField = new JTextField();
		tituloLibrotextField.setBounds(88, 10, 114, 19);
		contentPane.add(tituloLibrotextField);
		tituloLibrotextField.setColumns(10);

		lineaLibrotextField = new JTextField();
		lineaLibrotextField.setBounds(88, 37, 56, 19);
		contentPane.add(lineaLibrotextField);
		lineaLibrotextField.setColumns(10);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(39, 74, 283, 69);
		contentPane.add(scrollPane);

		resultadoLineatextArea = new JTextArea();
		scrollPane.setViewportView(resultadoLineatextArea);

		JLabel tituloLabel = new JLabel("Titulo: ");
		tituloLabel.setBounds(36, 12, 70, 15);
		contentPane.add(tituloLabel);

		JLabel lblNewLabel = new JLabel("Linea:");
		lblNewLabel.setBounds(39, 41, 70, 15);
		contentPane.add(lblNewLabel);

	}
	// DETALLES DEL EJERCICIO
	/*
	 * Partiendo del archivo indice.xml del ejercicio anterior,programar una clase
	 * Java llamada VerLineaLibro que solicite al usuario título de libro y número
	 * de línea y muestre el texto correspondiente a dicha línea dentro de ese
	 * libro. P.ej.:Introduzca título: Quijote Introduzca línea: 2 de cuyo nombre no
	 * quiero acordarme,Nota: Si el título o la línea dentro de ese título no
	 * existen en el índice de la biblioteca mostrar el mensaje de error que
	 * corresponda.Se realizarán dos versiones de este ejercicio, una basada en SAX
	 * y otra en DOM
	 */

	protected void buscarLineaLibro(JTextField tituloLibrotextField, JTextField lineaLibrotextField,
			JTextArea resultadoLineatextArea)
			throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

		if (tituloLibrotextField.getText().length() == 0 || lineaLibrotextField.getText().length() == 0) {
			JOptionPane.showMessageDialog(this, "Titulo o Línea están Vacíos\n debe rellenarlos.");
		} else {
			// Crea arbol DOM a partir de archivo XML
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.parse("/home/alumno/biblioteca/indice.xml");

			// Mediante una Expresion XPATH comprobamos si el titulo Introducido Existe o no
			// /biblioteca/libro[@titulo="TituloLibro"]/linea[@num=NumeroLinea]/text()
			// /linea[@num="+lineaLibrotextField.getText().trim()+"]/text()

			String titulo = tituloLibrotextField.getText().trim();

			Element contenidoLibro = (Element) (XPathFactory.newInstance().newXPath()
					.evaluate("/biblioteca/libro[@titulo='" + titulo + "']", doc, XPathConstants.NODE));

			if (contenidoLibro != null) {
				Element contenidoLineaLibro = (Element) (XPathFactory.newInstance().newXPath()
						.evaluate("/biblioteca/libro[@titulo='" + titulo + "']/linea[@num='"+lineaLibrotextField.getText().trim()+"']", doc, XPathConstants.NODE));
				
				int posicionLinea=Integer.valueOf(contenidoLineaLibro.getTextContent());
				
				mostrarContenidoLineaLibro(posicionLinea);
				
			} else {
				
				JOptionPane.showMessageDialog(this, "El libro con el titulo " + titulo + " no existe.");
				
			}

		}
	}

	private void mostrarContenidoLineaLibro(int posicionLinea) throws IOException {
		
		File rutaBiblioteca = new File("/home/alumno/biblioteca/"+tituloLibrotextField.getText().trim());
		
		RandomAccessFile RAF = new RandomAccessFile(rutaBiblioteca, "rw");
		
		String linea = "";
		
		RAF.seek(posicionLinea);
		
		linea = RAF.readLine();
		
		RAF.close();
		
		resultadoLineatextArea.setText(linea);
	}
}