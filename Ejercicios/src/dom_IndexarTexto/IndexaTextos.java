package dom_IndexarTexto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class IndexaTextos {

	public static void main(String[] args) {
		try {
			// Creamos el Arbol DOM
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.newDocument();

			// Crear Elemento Raiz
			Element raiz = doc.createElement("biblioteca");
			doc.appendChild(raiz);

			// File con la Ruta donde Almacenar
			File biblioteca = new File("/home/alumno/biblioteca/");
			File[] libros = biblioteca.listFiles();
						
			//Filtro en contenido del primer Array a un segundo Array para evitar
			//Que lea el propio fichero XML que se genera con la ejecucion de este
			//Programa
			ArrayList<File> listaLibro = new ArrayList<>();
			for (File file : libros) {
				if (!file.getName().equals("indice.xml")) {
					listaLibro.add(file);
				}
			}

			// Declaracion de Elementos y subelementos
			Element libro;
			Element linea = null;

			// Crea elemento hijo, aniade atributo, y lo cuelga de raiz
			for (int i = 0; i < listaLibro.size(); i++) {
				libro = doc.createElement("libro");
				libro.setAttribute("titulo", listaLibro.get(i).getName());
				raiz.appendChild(libro);
				
				//RAF para leer el Archivo y obtener el Puntero
				RandomAccessFile fichero = new RandomAccessFile(listaLibro.get(i), "rw");

				int contadorLineas = 1;

				// Crear elemento Linea y añadir elementos a Libro hasta terminar la lectura del
				// Fichero
				while ((fichero.readLine()) != null) {
					//La primera linea establece el valor de linea en 1 y su contenido a 0
					if (contadorLineas == 1) {
						linea = doc.createElement("linea");
						linea.setAttribute("num", String.valueOf(contadorLineas));
						linea.setTextContent("0");
						libro.appendChild(linea);
						contadorLineas++;
					}
					//Ya para el resto de lineas continua con la progresion y muestra el RAF de la linea
					linea = doc.createElement("linea");
					linea.setAttribute("num", String.valueOf(contadorLineas));
					libro.appendChild(linea);
					linea.setTextContent(String.valueOf(fichero.getFilePointer()));
					contadorLineas++;
				}
				//Esto nos permite eliminar la ultima linea que pertenece al EOF
				libro.removeChild(linea);
				fichero.close();
			}

			// Escribe arbol DOM a fichero XML
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("/home/alumno/biblioteca/indice.xml"));
			transformer.transform(source, result);

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}