package utilidades;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TransformarAMayusculas {

	public static void main(String[] args) throws IOException {
		
		if (args.length!=1) {
			System.err.println("¡Error! Debe introducir un Parametro");
			System.exit(-1);
		}
		
		if (!args[0].matches("[Mm]")) {
			System.err.println("¡Error! Debe introducir 'M' ó 'm' ");
			System.exit(-1);
		}
		
		boolean accion = args[0].equals("M");
		
		if (accion) {
			
			System.out.println("Ha seleccionado convertir a Mayusculas");
			
		}else {
			
			System.out.println("Ha seleccionado convertir a Minusculas");
			
		}
		
		System.out.print("Introduzca frase a convertir: ");
		
		BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
		
		String linea;
		
		while((linea=bfr.readLine())!=null){
			
			System.out.print("Palabra Transformada: ");
			System.out.println(accion?linea.toUpperCase():linea.toLowerCase());
			
			System.out.print("Introduzca frase a convertir: ");
			
		}
		
		bfr.close();
	}

}
