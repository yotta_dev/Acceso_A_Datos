package matrices;

import java.util.ArrayList;

public class Ejercicio_3_PK {

	public static void main(String[] args) {

		String pk_Actual = "0";
		String pk_Anterior = "0";
		int cnt = 1;
		ArrayList<Integer> puntosKilometricos = new ArrayList<>();

		try {

			while (!pk_Actual.equals("FIN")) {

				System.out.print("PK" + cnt + ": ");

				pk_Actual = utilidades.Entrada.cadena();

				if (Integer.valueOf(pk_Actual) < Integer.valueOf(pk_Anterior)) {

					pk_Actual = utilidades.Entrada.cadena();

				} else {

					puntosKilometricos.add(Integer.valueOf(pk_Actual));
					pk_Anterior = pk_Actual;
				}

				cnt++;
			}

		} catch (NumberFormatException e) {

			if (pk_Actual.equals("FIN")) {
				System.exit(0);
			} else {
				System.out.print("PK" + cnt + ": ");

				pk_Actual = utilidades.Entrada.cadena();
			}

		}

	}

}
