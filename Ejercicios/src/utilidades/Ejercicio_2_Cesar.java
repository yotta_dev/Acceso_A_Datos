package matrices;

import java.util.ArrayList;

public class Ejercicio_2_Cesar {

	public static void main(String[] args) {
		
		int claveCifrado;
		
		String palabraACifrar = "";
		
		String abecedario = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		
		String palabraCifrada = "";
		
		while(!palabraACifrar.equals("FIN")) {
			
			System.out.print("Introduzca un valor para el cifrado C�sar: ");
			claveCifrado = utilidades.Entrada.entero();
			
			System.out.print("Introduzca una palabra: ");
			palabraACifrar = utilidades.Entrada.cadena().trim().toUpperCase();
			
			for (int i = 0; i < palabraACifrar.length(); i++) {
				
				for(int j = 0; j < abecedario.length();j++) {
					
					if(abecedario.charAt(j) == palabraACifrar.charAt(i)) {
						int posicionLetraCifrado = j+claveCifrado;
						
						if (posicionLetraCifrado>=26) {
							posicionLetraCifrado = posicionLetraCifrado-26;
						}else {
							palabraCifrada += abecedario.charAt(posicionLetraCifrado);
						}
						
						//palabraCifrada += abecedario.charAt(abecedario.charAt(posicionCoincidencia+claveCifrado));
					}
				}				
				
			}
			
			System.out.println("La palabra: "+palabraACifrar+" con un cifrado C�sar clave :"+claveCifrado+" seria asi: "+palabraCifrada);
			palabraCifrada = "";
		}
		

	}

}
