package matrices;

public class Ejercicio_1_Matrices {

	public static void main(String[] args) {
		
        final int FILAS, COLUMNAS;
        
        int i, j, mayor, menor;
        int filaMayor, filaMenor, columnaMayor, columnaMenor;
        
        System.out.print("Introduzca el numero de filas: ");
        FILAS = utilidades.Entrada.entero();
        
        System.out.print("Introduzca el numero de Columnas por Fila: ");
        COLUMNAS = utilidades.Entrada.entero();
        
        int[][] matriz = new int[FILAS][COLUMNAS];//Inicializamos la Matriz con los valores indicados
        
        System.out.println("Introduzca n�mero en la Matriz: ");       
        for (i = 0; i < FILAS; i++) {
            for (j = 0; j < COLUMNAS; j++) {
                System.out.print("Posicion: [" + i + "][" + j + "]= ");//Mostramos la posicion en la que se va a almacenar el dato
                
                matriz[i][j] = utilidades.Entrada.entero();//Solicitamos numeros por teclado
                
            }
        }
        
        //Estructura de Bloque necesaria para rellenar una Matriz
        System.out.println("Valores introducidos en la Matriz:");
        for (i = 0; i < matriz.length; i++) { 
            for (j = 0; j < matriz[i].length; j++) {
                System.out.print(matriz[i][j] + " ");
            }        
            System.out.println();
        }
        
        mayor = menor = matriz[0][0]; //Inicializamos las variable mayor y menor con el dato introducido en la posicion 0.0
        filaMayor = filaMenor = columnaMayor = columnaMenor = 0; //Asi mismo con los numeros que almacenaran la siguiente informacion

        for (i = 0; i < matriz.length; i++) {  
        	
            for (j = 0; j < matriz[i].length; j++) {
            	
                if (matriz[i][j] > mayor) {//Vamos comparando casilla a casilla el valor de los datos introducidos con la variable Mayor
                	
                    mayor = matriz[i][j]; //En el caso de que sea mayor lo almacenamos en la variable Mayor
                    filaMayor = i; //Y a su vez la posicion de la 
                    columnaMayor = j;//Y de la columna a la que pertenece el nuevo dato mayor introducido
                    
                } else if (matriz[i][j] < menor) { //En caso de que no sea el Mayor comprobamos si el dato introducido es el menor existente
                	
                    menor = matriz[i][j];//Comparando con el dato menor almacenado
                    filaMenor = i;//Al igual que antes almacenamos el valor de la fila
                    columnaMenor = j;//Y de la columna para conocer la posicion del dato menor
                    
                }
            }           
        }
        System.out.println("Elemento mayor: " + mayor+" Posicion:["+filaMayor +"]["+columnaMayor+"] - Fila:"+ filaMayor + " Columna:" + columnaMayor);
        System.out.println("Elemento menor: " + menor+" Posicion:["+filaMenor +"]["+columnaMenor+"] - Fila:"+ filaMenor + " Columna:" + columnaMenor);

    }

}
