package utilidades;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class SeleccionarFichero {
	
private File seleccionarFichero() {
		
		File fichero = null;

		try {
			JFileChooser selectorDeFicheros = new JFileChooser();

			FileNameExtensionFilter filtroParaElSelector = new FileNameExtensionFilter(".txt", "txt", "text");

			selectorDeFicheros.setFileFilter(filtroParaElSelector);

			int op = selectorDeFicheros.showOpenDialog(selectorDeFicheros);

			fichero = selectorDeFicheros.getSelectedFile();

		} catch (NullPointerException ficheroNoSeleccionado) {

			JOptionPane.showMessageDialog(null, ficheroNoSeleccionado + "" + "\nNo se ha seleccionado un fichero",

					"AVISO!", JOptionPane.WARNING_MESSAGE);
		}

		return fichero;
	}
}
