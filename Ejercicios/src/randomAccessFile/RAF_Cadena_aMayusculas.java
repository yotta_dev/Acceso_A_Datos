package randomAccessFile;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class RAF_Cadena_aMayusculas extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldCadenaAConvertir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RAF_Cadena_aMayusculas frame = new RAF_Cadena_aMayusculas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RAF_Cadena_aMayusculas() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton buttonSeleccionar = new JButton("Seleccionar Fichero");
		buttonSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccionarFichero();
			}
		});
		buttonSeleccionar.setBounds(121, 52, 198, 25);
		contentPane.add(buttonSeleccionar);

		textFieldCadenaAConvertir = new JTextField();
		textFieldCadenaAConvertir.setBounds(187, 118, 229, 19);
		contentPane.add(textFieldCadenaAConvertir);
		textFieldCadenaAConvertir.setColumns(10);

		JLabel labelCadenaAConvertir = new JLabel("Cadena a Convertir : ");
		labelCadenaAConvertir.setBounds(25, 120, 158, 15);
		contentPane.add(labelCadenaAConvertir);

		JButton buttonConvertir = new JButton("Convertir");
		buttonConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File fichero = new File(
						"/home/alumno/Escritorio/Repositorios/Acceso_A_Datos/Ejercicios/src/randomAccessFile/TEXTO");

				try {

					convertirAMayusculas(fichero);

				} catch (FileNotFoundException e) {

					mostarMensajeEnPantalla("No se ha encontrado el fichero\n" + e.getMessage());

				} catch (IOException e) {

					mostarMensajeEnPantalla("Input Output Error\n" + e.getMessage());

				}
			}
		});
		buttonConvertir.setBounds(164, 178, 117, 25);
		contentPane.add(buttonConvertir);
	}

	public File seleccionarFichero() throws NullPointerException {

		File fichero = null;

		JFileChooser selectorDeFicheros = new JFileChooser();

		int op = selectorDeFicheros.showOpenDialog(this);

		if (op == JFileChooser.APPROVE_OPTION) {

			fichero = selectorDeFicheros.getSelectedFile();
		}

		return fichero;
	}

	private void convertirAMayusculas(File fichero) throws IOException {

		RandomAccessFile RAF = new RandomAccessFile(fichero, "rw");

		String linea = "";
		
		String palabraAConvertir=textFieldCadenaAConvertir.getText();
		
		Long posInit = RAF.getFilePointer();
		
		while ((linea = RAF.readLine()) != null) {
			
			if (linea.indexOf(textFieldCadenaAConvertir.getText()) != -1) {
				
				linea = linea.replace(palabraAConvertir, palabraAConvertir.toUpperCase());			
				
				RAF.seek(posInit);
				
				RAF.writeBytes(linea);
				
			}
		}
		
		RAF.close();
		
	}

	public void mostarMensajeEnPantalla(String mensaje) {
		JOptionPane.showMessageDialog(this, mensaje, "!AVISO!", JOptionPane.WARNING_MESSAGE);
	}
}
