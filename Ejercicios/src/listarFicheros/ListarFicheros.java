package listarFicheros;

import java.io.File;

public class ListarFicheros {

	public static void main(String[] args) {
		
		System.out.print("Introduzca la ruta del Directorio: ");

		String ruta = Entrada.cadena();
		
		listarFicheros(ruta);
	}

	private static void listarFicheros(String ruta) {
		int reset;
		
		do {
			reset = 0;
			
			File directorio = new File(ruta);

			if (directorio.exists()) {

				if (directorio.isDirectory()) {

					File[] contenidoDirectorio = directorio.listFiles();

					if (contenidoDirectorio != null) {
						for (int i = 0; i < contenidoDirectorio.length; i++) {

							if (contenidoDirectorio[i].isFile()) {
								System.out.println("-  Fichero   - "+contenidoDirectorio[i].getName());
							}

							if (contenidoDirectorio[i].isDirectory()) {
								System.out.println("- Directorio - "+contenidoDirectorio[i].getName());
								System.out.print("   -");
								listarFicheros(contenidoDirectorio[i].getAbsolutePath());
							}
							reset = 0;
						}
					} else {
						System.out.println("El Directorio Seleccionado esta Vacio");
					}
				} else {
					System.out.print("Introduzca una ruta que corresponda a un directorio: ");
					ruta = Entrada.cadena();
					reset = 1;
				}
			} else {
				System.out.print("Introduzca una ruta que Exista: ");
				ruta = Entrada.cadena();
				reset = 1;
			}
		} while (reset == 1);

	}

}
