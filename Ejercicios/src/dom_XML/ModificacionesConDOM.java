package dom_XML;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Utilizando DOM, realizar sobre él las siguientes modificaciones: Eliminar
 * todos los elementos <descripcion> Añadir al final un elemento
 * <total_calorias> que contenga la suma de las calorías de todos los desayunos
 * presentes en el documento XML.
 *
 * Cambiar la moneda de todos los precios a “euro” recalculando el precio según
 * los valores de las divisas almacenadas en un archivo existente Divisas.csv.
 * Este archivo se compone de líneas nombre_de_divisa;valor por ejemplo:
 * 
 * libra esterlina;0.87 peseta;166 dolar;1.12
 * 
 * Si una moneda no existe en el archivo Divisas.csv no se modificará el precio
 * del desayuno que corresponda.
 * 
 * @author Yehoshua
 *
 */

public class ModificacionesConDOM {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		// Crea arbol DOM a partir de archivo XML
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse("/home/alumno/Escritorio/TESTS/desayunos.xml");
		
		

	}

}
