package ej1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PersistenciaBD implements Persistencia {
	String tipoBD; // Tipo de Sistema Gestor de BD: mysql, derby, ...
	Connection cn; // Conexion con la BD

	public PersistenciaBD(String tipoBD, String ip, String usuario, String passwd, String baseDatos)
			throws ClassNotFoundException, SQLException {
		String cadenaConexion = "";
		this.tipoBD = tipoBD.toLowerCase();
		switch (this.tipoBD) {
		case "derby":
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			cadenaConexion = "jdbc:derby:" + baseDatos + ";create=true";
			cn = DriverManager.getConnection(cadenaConexion);
			break;
		case "mysql":
			Class.forName("com.mysql.jdbc.Driver");
			cadenaConexion = "jdbc:mysql://" + ip + "/" + baseDatos;
			cn = DriverManager.getConnection(cadenaConexion, usuario, passwd);
			break;
		default:
			throw new IllegalArgumentException(tipoBD + ": tipo base de datos desconocido");
		}
	}

	public void bajaPersona(String dni) throws SQLException {
		PreparedStatement ps = null;
		String prep = "DELETE FROM persona WHERE dni = ?";

		ps = cn.prepareStatement(prep);
		ps.setString(1, dni);
		ps.executeUpdate();
	}

	@Override
	public void desconectar() throws Exception {
		cn.close();
	}
	

	@Override
	public ArrayList<Persona> listadoPersonas(String tabla, String orderBy) throws SQLException {
		ArrayList<Persona> al=new ArrayList<Persona>();
		
		Statement st=cn.createStatement();
		String sql = "SELECT * FROM " + tabla + " "+ orderBy;
		ResultSet rs=st.executeQuery(sql);
		while (rs.next()) { 
			// Crea objeto Persona y lo añade a ArrayList
			al.add(new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"),rs.getString("email")));
		}
		return al;
	}

	@Override
	public void guardarPersona(String tabla, Persona p) throws Exception {
		//Comprueba si existe el email(clave primaria) para hacer UPDATE o INSERT
		boolean existe = consultarPersona(tabla, p.getEmail())!=null;
		String prep;
		if (existe)
			prep = "UPDATE " + tabla + " SET nombre=?,CP=?,pais=? WHERE email=?";
		else
			prep = "INSERT INTO " + tabla + " VALUES (?,?,?,?)";
		PreparedStatement ps = cn.prepareStatement(prep);
		ps.setString(1, p.getNombre());
		ps.setString(2, p.getCp());
		ps.setString(3, p.getPais());
		ps.setString(4, p.getEmail());
		ps.executeUpdate();
	}

	@Override
	public void borrarPersona(String tabla, String email) throws Exception {
		PreparedStatement ps = null;
		String prep = "DELETE FROM " + tabla + " WHERE email = ?";

		ps = cn.prepareStatement(prep);
		ps.setString(1, email);
		ps.executeUpdate();
	}

	@Override
	public Persona consultarPersona(String tabla, String email) throws Exception {
		// Comprueba que existe el email y devuelve objeto persona. null si no existe email
		PreparedStatement ps = null;
		String prep = "SELECT * FROM " + tabla + " WHERE email = ?";
		ResultSet rs;
		ps = cn.prepareStatement(prep);
		ps.setString(1, email);
		rs = ps.executeQuery();
		if (rs.next()) { // Existe
			// Crea objeto Persona a partir de las columnas obtenidas con SELECT.
			Persona p = new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"),
					rs.getString("email"));
			return p;
		} else { // No existe
			return null;
		}
	}
}
