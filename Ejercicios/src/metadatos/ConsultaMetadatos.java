package metadatos;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.DatabaseMetaData;
import com.mysql.jdbc.ResultSet;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ConsultaMetadatos extends JFrame {

	private JPanel contentPane;

	private JTextField textFieldNombreBD;
	private JTextField textFieldNombreTabla;

	private static Connection connection;
	private static DatabaseMetaData metaDatos;
	private JButton buttonComprobarCatalog;
	private JButton buttonComprobarTables;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				try {
					ConsultaMetadatos frame = new ConsultaMetadatos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {

					Class.forName("com.mysql.jdbc.Driver");

					connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ejercicios",
							"root", "manager");

					metaDatos = (DatabaseMetaData) connection.getMetaData();

				} catch (ClassNotFoundException eX) {
					// TODO Auto-generated catch block
					eX.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultaMetadatos() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 377, 192);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldNombreBD = new JTextField();
		textFieldNombreBD.setBounds(46, 23, 114, 22);
		contentPane.add(textFieldNombreBD);
		textFieldNombreBD.setColumns(10);

		textFieldNombreTabla = new JTextField();
		textFieldNombreTabla.setEnabled(false);
		textFieldNombreTabla.setColumns(10);
		textFieldNombreTabla.setBounds(46, 74, 114, 22);
		contentPane.add(textFieldNombreTabla);

		buttonComprobarCatalog = new JButton("Existe BD?");
		buttonComprobarCatalog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {

					existeBD();

				} catch (SQLException e) {

					e.printStackTrace();

				}
			}
		});
		buttonComprobarCatalog.setBounds(172, 21, 149, 25);
		contentPane.add(buttonComprobarCatalog);

		buttonComprobarTables = new JButton("Existe Tabla?");
		buttonComprobarTables.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					existeTabla();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
		buttonComprobarTables.setEnabled(false);
		buttonComprobarTables.setBounds(172, 72, 149, 25);
		contentPane.add(buttonComprobarTables);

		JButton buttonCancelar = new JButton("Cancelar");
		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelar();
			}
		});
		buttonCancelar.setBounds(122, 116, 117, 25);
		contentPane.add(buttonCancelar);
	}

	private void existeBD() throws SQLException {
		// CONSULTA BD
		ResultSet rsCatalogs = (ResultSet) metaDatos.getCatalogs();
		String bdABuscar = textFieldNombreBD.getText();
		ArrayList<String> basesDeDatos = new ArrayList<>();

		while (rsCatalogs.next()) {
			basesDeDatos.add(rsCatalogs.getString(1));
		}

		for (int i = 0; i < basesDeDatos.size(); i++) {

			if (basesDeDatos.get(i).equals(bdABuscar)) {

				JOptionPane.showMessageDialog(null, "La Base de Datos introducida, existe.", "Base de Datos Encontrada",
						JOptionPane.WARNING_MESSAGE);
				buttonComprobarTables.setEnabled(true);
				textFieldNombreTabla.setEnabled(true);
				return;

			}
		}

		JOptionPane.showMessageDialog(null, "La Base de Datos introducida, no existe.", "Base de Datos No Encontrada",
				JOptionPane.WARNING_MESSAGE);
		cancelar();

	}

	private void existeTabla() throws SQLException {
		// CONSULTA TABLA
		ResultSet rsTables = (ResultSet) metaDatos.getTables(null, null, "%", null);
		String tablaABuscar = textFieldNombreTabla.getText();
		ArrayList<String> tablas = new ArrayList<>();

		while (rsTables.next()) {

			tablas.add(rsTables.getString(3));

		}

		for (int i = 0; i < tablas.size(); i++) {

			if (tablas.get(i).equals(tablaABuscar)) {
				JOptionPane.showMessageDialog(null, "La Tabla introducida Existe.", "Tabla Encontrada",
						JOptionPane.WARNING_MESSAGE);
				cancelar();
				break;
			}

			JOptionPane.showMessageDialog(null, "La Tabla introducida No Existe.", "Tabla No Encontrada",
					JOptionPane.WARNING_MESSAGE);
			cancelar();

		}

	}

	private void cancelar() {

		buttonComprobarTables.setEnabled(false);
		textFieldNombreTabla.setEnabled(false);
		textFieldNombreTabla.setText("");
		textFieldNombreBD.setText("");
		textFieldNombreBD.grabFocus();

	}
}
