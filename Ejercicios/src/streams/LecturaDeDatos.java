package streams;

import java.io.IOException;

public class LecturaDeDatos {

	public static void main(String[] args) {

		int bytes;

		try {
			
			System.out.print("Introduzca una palabra o -1 para terminar: ");
			
			while((bytes= System.in.read())!=-1) {
			
			
				System.out.println("Byte: "+bytes+" Carácter: "+(char)bytes);
			}
			

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
