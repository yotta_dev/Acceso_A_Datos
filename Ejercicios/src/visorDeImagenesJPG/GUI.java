package visorDeImagenesJPG;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTextField;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldRutaImagen;
	private JTextField textField;
	private JTextField textField_1;
	private JLabel labelImagenIcon;
	private JPanel panelImagen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setTitle("Visor de Imagenes JPG");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 602, 532);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldRutaImagen = new JTextField();
		textFieldRutaImagen.setFont(new Font("Dialog", Font.PLAIN, 10));
		textFieldRutaImagen.setBounds(212, 30, 361, 25);
		contentPane.add(textFieldRutaImagen);
		textFieldRutaImagen.setColumns(10);

		JButton buttonSeleccionarImagen = new JButton("Seleccionar Imagen");
		buttonSeleccionarImagen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cargaInfoImagenSeleccionada(seleccionarImagen());
			}
		});
		buttonSeleccionarImagen.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonSeleccionarImagen.setBounds(35, 30, 165, 25);
		contentPane.add(buttonSeleccionarImagen);

		panelImagen = new JPanel();
		panelImagen.setBounds(35, 89, 545, 338);
		contentPane.add(panelImagen);
		panelImagen.setLayout(null);

		labelImagenIcon = new JLabel("");
		labelImagenIcon.setBackground(Color.WHITE);
		labelImagenIcon.setBounds(0, 0, 545, 338);
		panelImagen.add(labelImagenIcon);

		textField = new JTextField();
		textField.setEnabled(false);
		textField.setBounds(219, 448, 71, 19);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setEnabled(false);
		textField_1.setColumns(10);
		textField_1.setBounds(342, 448, 71, 19);
		contentPane.add(textField_1);

		JLabel lblAncho = new JLabel("Ancho:");
		lblAncho.setBounds(161, 450, 70, 15);
		contentPane.add(lblAncho);

		JLabel lblAlto = new JLabel("Alto:");
		lblAlto.setBounds(302, 450, 70, 15);
		contentPane.add(lblAlto);
	}

	private File seleccionarImagen() {

		JFileChooser selectorDeFicheros = new JFileChooser();

		selectorDeFicheros.setFileSelectionMode(JFileChooser.FILES_ONLY);

		FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter(".JPG", "jpg", "jpeg");

		selectorDeFicheros.setFileFilter(filtroImagen);

		int op = selectorDeFicheros.showOpenDialog(this);

		File directorioAListar = null;

		if (op == JFileChooser.APPROVE_OPTION) {

			directorioAListar = selectorDeFicheros.getSelectedFile();

		}

		return directorioAListar;
	}

	private void mostrarJOptionPane(String mensaje) {
		JOptionPane.showMessageDialog(null, mensaje, "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);

	}

	private void cargaInfoImagenSeleccionada(File imagenSeleccionada) {
		
		try {

			ImageIcon icon = new ImageIcon(imagenSeleccionada.toString());

			Icon icono = new ImageIcon(
					icon.getImage().getScaledInstance(labelImagenIcon.getWidth(), labelImagenIcon.getHeight(), Image.SCALE_DEFAULT));

			labelImagenIcon.setText(null);

			labelImagenIcon.setIcon(icono);
			
			mostrarAnchoYAlto(imagenSeleccionada);
			
		} catch (Exception ex) {

			JOptionPane.showMessageDialog(null, "Error abriendo la imagen " + ex);

		}

	}

	private void mostrarAnchoYAlto(File imagenSeleccionada) throws FileNotFoundException {
		
		BufferedReader reader = new BufferedReader(new FileReader(new File(imagenSeleccionada.getPath())));
		
	}
}
