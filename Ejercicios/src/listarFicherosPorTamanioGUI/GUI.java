package listarFicherosPorTamanioGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {

	// Componentes Graficos GUI
	private JPanel contentPane;
	private JTextField textFieldRutaSeleccionada;
	private JTextField textFieldBytes;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton buttonBuscar;
	private JTextArea textAreaResultado;
	private JRadioButton radioButtonMayores;
	private JRadioButton radioButtonMenores;

	// Variable Global para almacenar la Informacion de la Ruta seleccionada

	private JCheckBox checkBoxOcultos;
	private JCheckBox checkBoxSubcarpetas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 635, 453);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton buttonSeleccionar = new JButton("Seleccionar Carpeta");
		buttonSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccionarDirectorio();
			}
		});
		buttonSeleccionar.setBounds(32, 26, 201, 23);
		contentPane.add(buttonSeleccionar);

		textFieldRutaSeleccionada = new JTextField();
		textFieldRutaSeleccionada.setEditable(false);
		textFieldRutaSeleccionada.setBounds(245, 27, 335, 20);
		contentPane.add(textFieldRutaSeleccionada);
		textFieldRutaSeleccionada.setColumns(10);

		JLabel labelTamanio = new JLabel("Tama\u00F1o en Bytes");
		labelTamanio.setBounds(32, 68, 146, 14);
		contentPane.add(labelTamanio);

		textFieldBytes = new JTextField();
		textFieldBytes.setBounds(167, 66, 66, 20);
		contentPane.add(textFieldBytes);
		textFieldBytes.setColumns(10);

		radioButtonMayores = new JRadioButton("Mayores");
		radioButtonMayores.setSelected(true);
		buttonGroup.add(radioButtonMayores);
		radioButtonMayores.setBounds(245, 64, 90, 23);
		contentPane.add(radioButtonMayores);

		radioButtonMenores = new JRadioButton("Menores");
		buttonGroup.add(radioButtonMenores);
		radioButtonMenores.setBounds(245, 98, 90, 23);
		contentPane.add(radioButtonMenores);

		checkBoxOcultos = new JCheckBox("Incluir Ocultos");
		checkBoxOcultos.setBounds(338, 65, 145, 23);
		contentPane.add(checkBoxOcultos);

		checkBoxSubcarpetas = new JCheckBox("Incluir SubCarpetas");
		checkBoxSubcarpetas.setBounds(338, 98, 175, 23);
		contentPane.add(checkBoxSubcarpetas);

		buttonBuscar = new JButton("BUSCAR");
		buttonBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				realizarBusqueda();
			}
		});
		buttonBuscar.setEnabled(false);
		buttonBuscar.setBounds(491, 64, 89, 23);
		contentPane.add(buttonBuscar);

		JLabel labelResultado = new JLabel("Resultado de la B\u00FAsqueda: ");
		labelResultado.setBounds(32, 132, 232, 14);
		contentPane.add(labelResultado);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(32, 157, 548, 232);
		contentPane.add(scrollPane);

		textAreaResultado = new JTextArea();
		scrollPane.setViewportView(textAreaResultado);
	}

	/* Comienzo Metodos Creado Por el Programador */

	private void mostrarJOptionPane(String mensaje) {
		JOptionPane.showMessageDialog(null, mensaje, "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);

	}

	private void seleccionarDirectorio() {

		JFileChooser selectorDeFicheros = new JFileChooser();

		selectorDeFicheros.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int op = selectorDeFicheros.showOpenDialog(this);

		if (op == JFileChooser.APPROVE_OPTION) {
			// TODO Cambiar ruta por esto :selectorDeFicheros.getSelectedFile();

			File directorioAListar = new File(
					"/home/alumno/Escritorio/Repositorios/Acceso_A_Datos/Ejercicios/src/listarFicherosPorTamanioGUI/TEST/");

			textFieldRutaSeleccionada.setText(directorioAListar.getAbsolutePath());

			buttonBuscar.setEnabled(true);
		}

	}

	private void realizarBusqueda() {

		File carpetaDondeBuscar = new File(textFieldRutaSeleccionada.getText());

		Long tamEnBytes = Long.valueOf(textFieldBytes.getText());

		char criterio;

		if (radioButtonMayores.isSelected()) {
			criterio = '+';
		} else {
			criterio = '-';
		}

		boolean incluirOcultos = checkBoxOcultos.isSelected();

		boolean incluirSubCarpetas = checkBoxSubcarpetas.isSelected();

		ArrayList<File> resultado = buscarArchivosPorTamanio(carpetaDondeBuscar, tamEnBytes, criterio, incluirOcultos,
				incluirSubCarpetas);

		String textoResultado = "";

		if (resultado != null) {
			
			for (int i = 0; i < resultado.size(); i++) {

				if (resultado.get(i) == null) {

					textoResultado += "--------DENTRO DEL DIRECTORIO\n";

				} else {

					textoResultado += " - " + i + " Ruta: " + resultado.get(i).getAbsolutePath() + " - Tamaño: "
							+ resultado.get(i).length() + " Bytes\n";
				}
			}
			
			textAreaResultado.setText(textoResultado);
			
		} else {
			
			mostrarJOptionPane("0 Archivos Encontrados");
			
		}

		
	}

	ArrayList<File> buscarArchivosPorTamanio(File carpetaDondeBuscar, long tamEnBytes, char criterio,
			boolean incluirOcultos, boolean incluirSubcarpetas) {

		ArrayList<File> resultado = new ArrayList<>();

		ArrayList<File> resultado2 = new ArrayList<>();

		File[] contenidoDirectorio = carpetaDondeBuscar.listFiles();

		if (contenidoDirectorio == null) {
			return resultado;
		}

		for (int i = 0; i < contenidoDirectorio.length; i++) {

			// Solo los Mayores que >
			if (criterio == '+' && incluirOcultos == false) {

				if (contenidoDirectorio[i].length() > tamEnBytes) {

					// Filtramos para solo guardar los que no esten ocultos
					if (!contenidoDirectorio[i].isHidden()) {

						resultado.add(contenidoDirectorio[i]);

					}
				}
				// Mayores y Ficheros Ocultos
			} else {

				if (contenidoDirectorio[i].length() > tamEnBytes) {

					resultado.add(contenidoDirectorio[i]);

				}

			}

			// Solo los Menores que <
			if (criterio == '-' && incluirOcultos == false) {

				if (contenidoDirectorio[i].length() < tamEnBytes) {
					// Filtramos para solo guardar los que no esten ocultos
					if (!contenidoDirectorio[i].isHidden()) {

						resultado.add(contenidoDirectorio[i]);

					}
				}
				// Menores y Ocultos
			} else {

				if (contenidoDirectorio[i].length() < tamEnBytes) {

					resultado.add(contenidoDirectorio[i]);

				}

			}

			if (incluirSubcarpetas == true) {

				if (contenidoDirectorio[i].isDirectory()) {

					resultado.add(null);

					resultado2 = buscarArchivosPorTamanio(contenidoDirectorio[i], tamEnBytes, criterio, incluirOcultos,
							incluirSubcarpetas);

					if (resultado2 != null) {

						for (int j = 0; j < resultado2.size(); j++) {
							resultado.add(resultado2.get(j));
						}
					}
				}
			}
		}

		return resultado;

	}
}
