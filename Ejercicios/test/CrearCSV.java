package ej2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class CrearCSV {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		PrintWriter pw = null;

		System.out.print("Ruta del fichero donde se grabarán los registros: ");
		String aux = s.nextLine();
		File ruta = new File(aux);
		try {
			if (ruta.exists())
				//Abrimos fichero en modo append
				pw = new PrintWriter(new FileWriter(ruta,true));
			else{
				//Creamos fichero en modo append y escribimos linea cabecera
				pw = new PrintWriter(new FileWriter(ruta));
				pw.println("Matricula;Marca;Precio");
			}
			
			//Pedimos datos por teclado y los escribimos en fichero
			boolean fin=false;
			do {
				System.out.print("Matrícula (Intro para salir): ");
				String matricula = s.nextLine();
				fin= matricula.length()==0;
				if (!fin){
					System.out.print("Marca: : ");
					String marca = s.nextLine();
					System.out.print("Precio: : ");
					String precio = s.nextLine();
					//Escribimos los datos leidos en el archivo CSV separados por ;
					pw.println(matricula+";"+marca+";"+precio);
				}
			} while (!fin);
			//Cerramos fichero
			pw.close();
		} catch (IOException e) {
			System.out.println("Error al abrir "+ruta);
			e.printStackTrace();
		}
		System.out.println("=== FIN DE PROGRAMA ===");
	}

}
