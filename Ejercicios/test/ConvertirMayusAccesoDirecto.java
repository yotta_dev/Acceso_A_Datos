package ej7;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.RandomAccessFile;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;




public class ConvertirMayusAccesoDirecto extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField ruta;
	private JTextField cadena;
	private JFrame ventanaGUI=this;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConvertirMayusAccesoDirecto frame = new ConvertirMayusAccesoDirecto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConvertirMayusAccesoDirecto() {
		setTitle("Convertir a may\u00FAsculas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 494, 271);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnSeleccionar = new JButton("Seleccionar Texto");
		btnSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc=new JFileChooser("/");
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int seleccion = fc.showOpenDialog(ventanaGUI);
				if (seleccion == JFileChooser.APPROVE_OPTION)
				{
					ruta.setText(fc.getSelectedFile().getAbsolutePath());
				}
			}
		});
		btnSeleccionar.setBounds(43, 23, 175, 23);
		contentPane.add(btnSeleccionar);
		
		ruta = new JTextField();
		ruta.setEditable(false);
		ruta.setBounds(156, 70, 298, 20);
		contentPane.add(ruta);
		ruta.setColumns(10);
		
		JLabel lblTamaoEnBytes = new JLabel("Cadena a convertir");
		lblTamaoEnBytes.setBounds(43, 122, 113, 14);
		contentPane.add(lblTamaoEnBytes);
		
		cadena = new JTextField();
		cadena.setBounds(156, 119, 185, 20);
		contentPane.add(cadena);
		cadena.setColumns(10);
		
		JButton btnConvertir = new JButton("CONVERTIR");
		btnConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					String r=ruta.getText().trim();
					String cad=cadena.getText().trim();
					
					if (r.isEmpty()||cad.isEmpty()){
						JOptionPane.showMessageDialog(ventanaGUI, "Faltan par�metros", "Debe seleccionar un fichero e indicar una cadena a convertir.", JOptionPane.ERROR_MESSAGE);
						return;
					}
					
					try {
						int n=convertirAMayuscula(r,cad);
						JOptionPane.showMessageDialog(ventanaGUI,"Realizadas conversiones en "+n+" l�neas");
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(ventanaGUI, "Error al convertir", e1.getMessage(), JOptionPane.ERROR_MESSAGE);
					}
			}
		});
		btnConvertir.setBounds(43, 175, 175, 23);
		contentPane.add(btnConvertir);
		
		JLabel lblRutaDelArchivo = new JLabel("Ruta del archivo");
		lblRutaDelArchivo.setBounds(43, 73, 113, 14);
		contentPane.add(lblRutaDelArchivo);
	}

	int convertirAMayuscula(String ruta, String patron) throws IOException {
		RandomAccessFile raf;
		long pos;
		int cnt = 0;

		raf = new RandomAccessFile(ruta, "rw");
		//Guardamos la posicion de comiendo de cada linea (antes de leerla)
		pos=raf.getFilePointer();
		String linea;
		while ((linea=raf.readLine()) != null) {
			if (linea.indexOf(patron)!=-1){
				//Reemplazamos la cada ocurrencia de cad por su may�scula
				linea=linea.replace(patron,patron.toUpperCase());
				raf.seek(pos);
				//Se escribe la linea con el separador de cada sistema. "\n" en Unix y "\r\n" en Windows
				raf.writeBytes(linea+System.lineSeparator());
				cnt++;
			}
			
			pos=raf.getFilePointer();
		}
		raf.close();
		
		return cnt;
	}
}
