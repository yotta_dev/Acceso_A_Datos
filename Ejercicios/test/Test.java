package test;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Connection;

import ej1.Persona;

public class Test {
	Connection cn;

	public static void main(String[] args) throws IOException, SQLException {
		
		//LecturaFicheros
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Test.class.getResourceAsStream("rutaFicheroALeer")));
		
		String linea;
		String texto = null;
		
		while ((linea = bufferedReader.readLine())!=null) {			
			texto += linea +"\n"; 			
		}
		
		//Escritura Ficheros
		PrintWriter printWriter = new PrintWriter(new FileWriter("rutaFicheroAEscribir"));
				
		//JOPTION PANE
		JOptionPane.showMessageDialog(null, "MensajeAMostrar", "CabeceraAMostrar", JOptionPane.WARNING_MESSAGE);
				
		if (JOptionPane.showConfirmDialog(null, "Are you sure?", "WARNING",
		        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
		    // yes option
		} else {
		    // no option
		}
		
		//Conexion BD
		try {
		    Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
		    System.err.println("No se encontro el Driver MySQL para JDBC.");
		}
		
		Connection cn = (Connection) DriverManager.getConnection("jdbc:mysql://servidor_bd:puerto/nombre_bd", "usuario", "contraseņa");
	
	}
	
	public void guardarPersona(String tabla, Persona persona) throws Exception {
		//Comprueba si existe el email(clave primaria) para hacer UPDATE o INSERT
		boolean existe=true;// = consultarPersona(tabla, p.getEmail())!=null;
		String prep;
		if (existe)
			prep = "UPDATE " + tabla + " SET nombre=?,CP=?,pais=? WHERE email=?";
		else
			prep = "INSERT INTO " + tabla + " VALUES (?,?,?,?)";
		PreparedStatement ps = cn.prepareStatement(prep);
		ps.setString(1, persona.getNombre());
		ps.setString(2, persona.getCp());
		ps.setString(3, persona.getPais());
		ps.setString(4, persona.getEmail());
		ps.executeUpdate();
	}
	
	public Persona consultarPersona(String tabla, String email) throws Exception {
		// Comprueba que existe el email y devuelve objeto persona. null si no existe email
		PreparedStatement ps = null;
		String prep = "SELECT * FROM " + tabla + " WHERE email = ?";
		ResultSet rs;
		ps = cn.prepareStatement(prep);
		ps.setString(1, email);
		rs = ps.executeQuery();
		if (rs.next()) { // Existe
			// Crea objeto Persona a partir de las columnas obtenidas con SELECT.
			Persona p = new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"),
					rs.getString("email"));
			return p;
		} else { // No existe
			return null;
		}
	}

}
