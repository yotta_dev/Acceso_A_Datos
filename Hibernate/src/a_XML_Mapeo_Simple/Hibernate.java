package a_XML_Mapeo_Simple;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Hibernate {

	public static void main(String[] args) {

		SessionFactory sessionFactory;

		Configuration configuration = new Configuration();
		
		configuration.configure("hibernate_XML/hibernate.cfg.xml");
		
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
		
		Profesor profesor=new Profesor(101, "Juan", "Perez", "García");  //Creamos el objeto
		 
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		 
		session.save(profesor); //<|--- Aqui guardamos el objeto en la base de datos.
		 
		session.getTransaction().commit();
		
		session.close();

		//Cerramos Factory Session
		sessionFactory.close();

	}

}
