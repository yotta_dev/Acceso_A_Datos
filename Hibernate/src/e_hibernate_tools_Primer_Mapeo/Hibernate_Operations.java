package e_hibernate_tools_Primer_Mapeo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import e_primer_Mapeo_Banco.Cliente;

public class Hibernate_Operations {

	public static void main(String[] args) {

		// Creamos la Sesion Factory Para poder realizar la conexion durante la
		// siguiente sesion
		SessionFactory sessionFactory;
		// Objeto Configuracion para almacenar la configuracion de la conexion
		Configuration configuration = new Configuration();
		// Lo enlazamos al fichero de configuracion de Hibernate
		configuration.configure("e_hibernate_tools_Primer_Mapeo/hibernate.cfg.xml");

		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();

		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		
		//START CONSTRUIR OBJECTOS CON LOS QUE REALIZAR LAS TRANSACCIONES
		//Date miFecha = new Date(año,mes,dia)
		Date fecha = new Date();			
		
		e_primer_Mapeo_Banco.Cliente cliente = new Cliente("Yehoshua",fecha,"C/Calle","06018454R");
						      
      
		//END CONSTRUIR OBJECTOS CON LOS QUE REALIZAR LAS TRANSACCIONES

		// INICIO DE LAS TRANSACCIONES
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		// Start Operaciones CRUD

		session.saveOrUpdate(cliente);

		// End Operaciones CRUD
		session.getTransaction().commit();
		session.close();
		// FIN DE LAS TRANSACCIONES

	}

}
