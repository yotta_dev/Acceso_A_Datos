package d_XML_Relaciones_N_N_Desordenado;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Main_Tester {

	public static void main(String[] args) {

		Profesor profesor1 = new Profesor(11, "Isabel", "Fuertes", "Gascón");
		Profesor profesor2 = new Profesor(12, "Jose", "Valenciano", "Gimeno");
		
		Modulo modulo1 = new Modulo(1, "Sistemas Operativos en Red");
		Modulo modulo2 = new Modulo(2, "Entornos de desarrollo");
		Modulo modulo3 = new Modulo(3, "Sistemas Informáticos");

		profesor1.getModulos().add(modulo1);
		profesor1.getModulos().add(modulo2);
		profesor2.getModulos().add(modulo3);

		modulo1.getProfesores().add(profesor1);
		modulo2.getProfesores().add(profesor1);
		modulo3.getProfesores().add(profesor2);
		
		//Abrimos SessionFactory
		SessionFactory sessionFactory;
		//Creamos la Configuracion
        Configuration configuration = new Configuration();
        //Cargamos el Fichero CFG HBM con la configuracion de Mapeo de la BD
        configuration.configure("d_XML_Relaciones_N_N_Desordenado/hibernate.cfg.xml");
        //Se aplica la configuracion del Servicio de Registro
        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        //Abrimos la Sesion
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		//Realizamos las operaciones pertinentes
		session.save(profesor1);
		session.save(profesor2);
		//Hacemos Commit de las acciones realizadas y cerramos la conexion
		session.getTransaction().commit();
		session.close();

	}

}
