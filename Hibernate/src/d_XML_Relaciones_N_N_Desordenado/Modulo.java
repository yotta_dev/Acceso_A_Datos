package d_XML_Relaciones_N_N_Desordenado;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Modulo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//ATRIBUTOS
	private int idModulo;
	private String nombre;
	private Set<Profesor> profesores = new HashSet<Profesor>();
	
	//CONSTRUCTOR VACIO
	public Modulo() {

	}
	
	//CONSTRUCTOR CON ARGUMENTOS
	public Modulo(int idModulo, String nombre) {
		this.idModulo = idModulo;
		this.nombre = nombre;

	}
	
	//GETTERS AND SETTERS
	public int getIdModulo() {
		return idModulo;
	}

	public void setIdModulo(int idModulo) {
		this.idModulo = idModulo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Set<Profesor> getProfesores() {
		return profesores;
	}

	public void setProfesores(Set<Profesor> profesores) {
		this.profesores = profesores;
	}	
	
}